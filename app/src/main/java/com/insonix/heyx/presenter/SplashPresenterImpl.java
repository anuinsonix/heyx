package com.insonix.heyx.presenter;


import android.os.Handler;

import com.insonix.heyx.view.activity.SplashView;

/**
 * Created by insonix on 24/11/16.
 */
public class SplashPresenterImpl implements SplashPresenter {

    SplashView mView;

    public SplashPresenterImpl(SplashView splashView) {
        this.mView=splashView;
    }

    @Override
    public void runWaitThread(int timeUnits) {

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                mView.onDelayCompletion();
            }
        }, timeUnits);
    }

    @Override
    public void registerDevice(String deviceId) {

    }
}
