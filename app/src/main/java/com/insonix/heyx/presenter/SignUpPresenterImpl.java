package com.insonix.heyx.presenter;

import android.util.Log;

import com.insonix.heyx.R;
import com.insonix.heyx.api.APIClient;
import com.insonix.heyx.api.ApiEndpointInterface;
import com.insonix.heyx.model.Profile;
import com.insonix.heyx.model.User;
import com.insonix.heyx.utils.MyPreferences;
import com.insonix.heyx.view.activity.SignUpView;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;
import static com.insonix.heyx.utils.MyApplication.OS;
import static com.insonix.heyx.utils.MyApplication.mPrefs;


/**
 * Created by insonix on 25/11/16.
 */
public class SignUpPresenterImpl implements SignUpPresenter {

    SignUpView mView;

    private String contactSyn = "0", callSyn = "0", msgSyn = "0";
    public SignUpPresenterImpl(SignUpView mView) {
        this.mView = mView;
    }


    @Override
    public void onSubmit(String name, String password, String mCpwd,String token,String androidId, String phone) {
        if (onValidate(name, password, mCpwd)) {
            mView.onShowProgress();
            ApiEndpointInterface apiService =
                    APIClient.getClient().create(ApiEndpointInterface.class);
            Call<User> call = apiService.registerUser(name,phone,mCpwd, OS, token,androidId);
            call.enqueue(new Callback<User>() {
                @Override
                public void onResponse(Call<User> call, Response<User> response) {
                    Log.d(TAG, "onResponse: "+response.raw());
                    mView.onHideProgress();
                    Log.d("response","response"+response.raw());

                    if(response.body().getStatus().equals("true")){

                        mView.showSuccessMsg(R.string.success);
                      //  mView.moveToNext();
                        response.body().getUserid();
                        mView.moveToNext(response.body());

                    }else{
                        mView.showError(response.body().getMessage());
                    }

                }

                @Override
                public void onFailure(Call<User> call, Throwable t) {
                    mView.onHideProgress();
                    mView.showError();
                }
            });
//            mView.start();
        }
    }

    @Override
    public void getProfile(String phone) {
        ApiEndpointInterface apiService =
                APIClient.getClient().create(ApiEndpointInterface.class);
        Call<Profile> call = apiService.getProfile(phone);
        call.enqueue(new Callback<Profile>() {
            @Override
            public void onResponse(Call<Profile> call, Response<Profile> response) {
                if(response.body().getStatus().equalsIgnoreCase("true")){
                    mView.setProfile(response.body().getUsername());
                }
            }
            @Override
            public void onFailure(Call<Profile> call, Throwable t) {

            }
        });
    }

    private boolean onValidate(String name, String password, String mCpwd) {
        if (name.isEmpty()) {
            mView.showNameError();
            return false;
        }
        if (password.isEmpty() ) {
            mView.showPasswordError();
            return false;
        }
        if(mCpwd.isEmpty()){
            mView.showConfirmPasswordError();
            return false;
        }
        if (password.length() < 4) {
            mView.showPasswordLengthError();
            return false;
        }
        if (!password.equals(mCpwd)) {
            mView.showPasswordMismatchError();
            return false;
        }
        return true;
    }

    @Override
    public void showProgressDialog() {

    }

    @Override
    public void hideProgressDialog() {

    }

    @Override
    public void hideKeyboard() {

    }

    @Override
    public void showInternetError() {

    }

    @Override
    public void ApiFailureError() {

    }

    @Override
    public void timeOutError() {

    }

}
