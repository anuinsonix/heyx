package com.insonix.heyx.presenter;

import android.util.Log;

import com.insonix.heyx.api.APIClient;
import com.insonix.heyx.api.ApiEndpointInterface;
import com.insonix.heyx.model.SyncResponse;
import com.insonix.heyx.view.activity.Homeview;

import java.net.UnknownHostException;

import javax.net.ssl.SSLHandshakeException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by insonix on 25/11/16.
 */
public class HomePresenterImpl implements HomePresenter {

    Homeview mView;

    public HomePresenterImpl(Homeview mView) {
        this.mView = mView;

    }



    @Override
    public void showProgressDialog() {
        mView.onShowProgress();
    }

    @Override
    public void hideProgressDialog() {
        mView.onHideProgress();
    }

    @Override
    public void hideKeyboard() {

    }

    @Override
    public void showInternetError() {

    }

    @Override
    public void ApiFailureError() {

    }

    @Override
    public void timeOutError() {

    }

    @Override
    public void getSelectedService(String userid) {
        if (mView.isInternetOk()) {
            ApiEndpointInterface apiService =
                    APIClient.getClient().create(ApiEndpointInterface.class);
            mView.onShowProgress();
            Call call = apiService.getSyncSettings(userid);
            call.enqueue(new Callback<SyncResponse>() {
                @Override
                public void onResponse(Call<SyncResponse> call, Response<SyncResponse> response) {
                    mView.onHideProgress();
                    Log.d("response","response1"+response.raw());
    //                Timber.i("Response" + response.body().getStatus());
                    if (response!=null && response.body().getStatus().equalsIgnoreCase("true")) {
                        mView.changeView(response.body().getSycData());
                    } else {
                        mView.showError("Unknown error occurred..please try again!!!");
                    }
                }

                @Override
                public void onFailure(Call<SyncResponse> call, Throwable t) {
                    mView.onHideProgress();
                    mView.showError("Unable to resolve DNS");
                }
            });
        } else {
            mView.showUnavailable();
        }
    }
}
