package com.insonix.heyx.presenter;

import android.util.Log;

import com.insonix.heyx.api.APIClient;
import com.insonix.heyx.api.ApiEndpointInterface;
import com.insonix.heyx.model.NotificationEnableResponse;
import com.insonix.heyx.view.NotificationEnableView;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by insonix on 6/12/17.
 */

public class NotificationEnablePresenterImpl implements NotificationEnablePresenter{

    private final NotificationEnableView mView;
    public NotificationEnablePresenterImpl(NotificationEnableView mView){
        this.mView=mView;
    }

    @Override
    public void enableNotification(String userid, String not_status) {
        if (mView.isInternetOk()) {
            mView.onShowProgress();
            ApiEndpointInterface apiService =
                    APIClient.getClient().create(ApiEndpointInterface.class);
            Call call=apiService.enableNotification(userid,not_status);
            call.enqueue(new Callback<NotificationEnableResponse>() {
                @Override
                public void onResponse(Call<NotificationEnableResponse> call, Response<NotificationEnableResponse> response) {
                    mView.onHideProgress();
                    Log.d("response","response"+response.raw());
                    if (response.body().getStatus().equalsIgnoreCase("true")) {
                        mView.onNotificationEnable(response.body());
                    } else {
                        mView.showError("Unknown error occurred..please try again!!!");
                    }
                }

                @Override
                public void onFailure(Call<NotificationEnableResponse> call, Throwable t) {
                    mView.onHideProgress();
                }
            });

        } else {
            mView.onHideProgress();
            mView.showError("No internet connection available");
        }
    }
}
