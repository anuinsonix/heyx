package com.insonix.heyx.presenter;

/**
 * Created by insonix on 6/12/17.
 */

public interface NotificationEnablePresenter {
    void enableNotification(String userid,String not_status);
}
