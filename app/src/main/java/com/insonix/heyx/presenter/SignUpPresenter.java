package com.insonix.heyx.presenter;

/**
 * Created by insonix on 25/11/16.
 */

public interface SignUpPresenter extends WebPresenter {


    void onSubmit(String name, String password, String mCpwd,String token, String androidId, String id);

    void getProfile(String phone);

}
