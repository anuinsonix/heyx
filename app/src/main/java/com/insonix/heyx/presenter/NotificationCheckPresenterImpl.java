package com.insonix.heyx.presenter;

import android.util.Log;

import com.insonix.heyx.api.APIClient;
import com.insonix.heyx.api.ApiEndpointInterface;
import com.insonix.heyx.model.NotificationCheckEnableResponse;
import com.insonix.heyx.model.NotificationEnableResponse;
import com.insonix.heyx.view.NotificationCheckEnableView;
import com.insonix.heyx.view.NotificationEnableView;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;

/**
 * Created by insonix on 6/12/17.
 */

public class NotificationCheckPresenterImpl implements NotificationCheckPresenter{

    private final NotificationCheckEnableView mView;
    public NotificationCheckPresenterImpl(NotificationCheckEnableView mView){
        this.mView=mView;
    }

    @Override
    public void checkenablNotification(String userid) {
        if (mView.isInternetOk()) {
            mView.onShowProgress();
            ApiEndpointInterface apiService =
                    APIClient.getClient().create(ApiEndpointInterface.class);
            Call call=apiService.checkenablNotification(userid);
            call.enqueue(new Callback<NotificationCheckEnableResponse>() {
                @Override
                public void onResponse(Call<NotificationCheckEnableResponse> call, Response<NotificationCheckEnableResponse> response) {
                    mView.onHideProgress();
                    Log.d(TAG,"response111"+response.raw());
                    if (response.body().getStatus().equalsIgnoreCase("true")) {
                        mView.onNotificationcheckEnable(response.body());
                    } else {
                        mView.showError("Unknown error occurred..please try again!!!");
                    }
                }

                @Override
                public void onFailure(Call<NotificationCheckEnableResponse> call, Throwable t) {
                    Log.d(TAG, "onFailure:111 "+t);
                    mView.onHideProgress();
                }
            });

        } else {
            mView.showError("No internet connection available");
        }
    }
}
