package com.insonix.heyx.presenter;

import android.util.Log;

import com.insonix.heyx.R;
import com.insonix.heyx.api.APIClient;
import com.insonix.heyx.api.ApiEndpointInterface;
import com.insonix.heyx.model.GenericResponse;
import com.insonix.heyx.model.SyncResponse;
import com.insonix.heyx.view.fragment.SettingsView;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by insonix on 1/12/16.
 */
public class SettingsPresenterImpl implements SettingsPresenter {

    SettingsView mView;

    public SettingsPresenterImpl(SettingsView mView) {
        this.mView = mView;
    }


    @Override
    public void getSyncSettings(String userid) {

        if (mView.isInternetOk()) {
            mView.onShowProgress();
            ApiEndpointInterface apiService =
                    APIClient.getClient().create(ApiEndpointInterface.class);
            Call call = apiService.getSyncSettings(userid);
            call.enqueue(new Callback<SyncResponse>() {
                @Override
                public void onResponse(Call<SyncResponse> call, Response<SyncResponse> response) {
                    mView.onHideProgress();
                    Log.d("response","response"+response.raw());
                    Timber.i("Response" + response.body().getStatus());
                    if (response.body().getStatus().equalsIgnoreCase("true")) {
                        if(!response.body().getSycData().toString().equals("[]")){
                            mView.setCheckedButtons(response.body().getSycData());
                        }
                    } else {
                        mView.showError("Unknown error occurred..please try again!!!");
                    }
                }

                @Override
                public void onFailure(Call<SyncResponse> call, Throwable t) {
                    mView.onHideProgress();
                    mView.showError("Unknown error occurred..please try again!!!");
                }
            });
        } else {
            mView.showError("No internet connection available");
        }
    }

    @Override
    public void updateSettings(String userid, String contactSyn, String msgSyn, String callSyn) {
        if (mView.isInternetOk()) {
            mView.onShowProgress();
            ApiEndpointInterface apiService =
                    APIClient.getClient().create(ApiEndpointInterface.class);
//        mView.onShowProgress();
            Call call = apiService.setSyncSettings(userid,contactSyn,msgSyn,callSyn);
            call.enqueue(new Callback<GenericResponse>() {
                @Override
                public void onResponse(Call<GenericResponse> call, Response<GenericResponse> response) {
    //                mView.onHideProgress();
                    Timber.i("Response" + response.body().getStatus());
                    Log.d("response","response"+response.raw());
                    mView.onHideProgress();
                    mView.onBack();
                    if (response.body().getStatus().equalsIgnoreCase("true")) {
                        mView.showSuccessMsg(R.string.success_msg);
                        mView.startFragment();
                    } else {
                        mView.showError("Unknown error occurred..please try again!!!");
                    }
                }

                @Override
                public void onFailure(Call<GenericResponse> call, Throwable t) {
                    mView.onHideProgress();
                    mView.showError(t.getMessage());
                }
            });
        }else{
            mView.showError("No internet connection available");
        }
    }

    @Override
    public void showProgressDialog() {

    }

    @Override
    public void hideProgressDialog() {

    }

    @Override
    public void hideKeyboard() {

    }

    @Override
    public void showInternetError() {

    }

    @Override
    public void ApiFailureError() {

    }

    @Override
    public void timeOutError() {

    }
}
