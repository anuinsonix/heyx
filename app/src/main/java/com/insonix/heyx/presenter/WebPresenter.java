package com.insonix.heyx.presenter;

/**
 * Created by insonix on 24/11/16.
 */
public interface WebPresenter {

    void showProgressDialog();

    void hideProgressDialog();

    void hideKeyboard();

    void showInternetError();

    void ApiFailureError();

    void timeOutError();

}
