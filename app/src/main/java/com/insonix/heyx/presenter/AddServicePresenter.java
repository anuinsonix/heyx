package com.insonix.heyx.presenter;

import android.support.annotation.StringRes;

import com.insonix.heyx.model.RequestArray;
import com.insonix.heyx.model.SyncResponse;

/**
 * Created by insonix on 25/11/16.
 */
public interface AddServicePresenter {

    void addService(String tag, String phonenumber);

    void updateService(String phonenumber1, SyncResponse.SycDataBean sycData);

    void getSelectedService(String phone);


    void addAllServices(String phonenumber);
}
