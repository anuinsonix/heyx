package com.insonix.heyx.presenter;

import android.util.Log;

import com.insonix.heyx.api.APIClient;
import com.insonix.heyx.api.ApiEndpointInterface;
import com.insonix.heyx.model.LoggedBean;
import com.insonix.heyx.model.SyncResponse;
import com.insonix.heyx.view.fragment.LoggedInView;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by insonix on 19/12/16.
 */
public class LoggedInPresenterImpl implements LoggedInPresenter {

    LoggedInView mView;

    public LoggedInPresenterImpl(LoggedInView mView) {
        this.mView = mView;
    }


    @Override
    public void getLastLoggedInDetails(String userid) {
        if (mView.isInternetOk()) {
            ApiEndpointInterface apiService =
                    APIClient.getClient().create(ApiEndpointInterface.class);
            mView.onShowProgress();
            Call call = apiService.getLoggedInDetails(userid);
            call.enqueue(new Callback<LoggedBean>() {
                @Override
                public void onResponse(Call<LoggedBean> call, Response<LoggedBean> response) {
                    mView.onHideProgress();
                    Log.d("response","response"+response.raw());
                    if (response != null && response.body().getStatus().equalsIgnoreCase("true")) {
                        mView.showLastLoggedInSessions(response.body());
                    } else {
                        mView.showError("Unknown error occurred..please try again!!!");
                    }
                }

                @Override
                public void onFailure(Call<LoggedBean> call, Throwable t) {
                    mView.onHideProgress();
                    mView.showError("Unable to resolve DNS");
                }
            });
        } else {
            mView.showUnavailable();
        }
    }

    @Override
    public void showProgressDialog() {

    }

    @Override
    public void hideProgressDialog() {

    }

    @Override
    public void hideKeyboard() {

    }

    @Override
    public void showInternetError() {

    }

    @Override
    public void ApiFailureError() {

    }

    @Override
    public void timeOutError() {

    }
}
