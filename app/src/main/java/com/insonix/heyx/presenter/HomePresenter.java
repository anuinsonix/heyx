package com.insonix.heyx.presenter;

/**
 * Created by insonix on 25/11/16.
 */

public interface HomePresenter extends WebPresenter{
    void getSelectedService(String userid);
}
