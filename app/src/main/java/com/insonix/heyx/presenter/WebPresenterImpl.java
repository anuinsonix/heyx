package com.insonix.heyx.presenter;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.ProgressBar;

import com.insonix.heyx.R;
import com.insonix.heyx.utils.MyApplication;
import com.insonix.heyx.view.activity.Baseview;

/**
 * Created by insonix on 24/11/16.
 */
public class WebPresenterImpl implements WebPresenter {

    Baseview mView;
    ProgressDialog progressDialog;

    public WebPresenterImpl(Baseview webView) {
        this.mView = webView;
    }

    @Override
    public void showProgressDialog() {

        progressDialog = new ProgressDialog(MyApplication.context);
        progressDialog.setMessage(MyApplication.context.getString(R.string.loading));
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    @Override
    public void hideProgressDialog() {
        progressDialog.hide();
    }

    @Override
    public void hideKeyboard() {
        mView.onHideKeyboard();
    }

    @Override
    public void showInternetError() {
        mView.showError("Internet not available");
    }

    @Override
    public void ApiFailureError() {
        mView.showError("Internet not available");
    }


    @Override
    public void timeOutError() {
        mView.showError("Request timed out");
    }

}
