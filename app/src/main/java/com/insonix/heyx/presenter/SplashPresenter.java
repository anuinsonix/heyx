package com.insonix.heyx.presenter;

/**
 * Created by insonix on 24/11/16.
 */

public interface SplashPresenter {

    void runWaitThread(int timeUnits);

    void registerDevice(String deviceId);

}
