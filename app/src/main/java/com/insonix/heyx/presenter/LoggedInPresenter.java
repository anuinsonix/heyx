package com.insonix.heyx.presenter;

/**
 * Created by insonix on 19/12/16.
 */

public interface LoggedInPresenter extends WebPresenter {

    void getLastLoggedInDetails(String phone);

}
