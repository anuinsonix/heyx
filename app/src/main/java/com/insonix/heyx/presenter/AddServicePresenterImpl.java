package com.insonix.heyx.presenter;

import android.util.Log;

import com.google.gson.Gson;
import com.insonix.heyx.R;
import com.insonix.heyx.api.APIClient;
import com.insonix.heyx.api.ApiEndpointInterface;
import com.insonix.heyx.model.GenericResponse;
import com.insonix.heyx.model.RequestArray;
import com.insonix.heyx.model.SyncResponse;
import com.insonix.heyx.utils.CommonUtils;
import com.insonix.heyx.view.activity.AddServiceView;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

import static com.insonix.heyx.utils.Constants.CALL_TAG;
import static com.insonix.heyx.utils.Constants.CONTACTS_TAG;
import static com.insonix.heyx.utils.Constants.SMS_TAG;

/**
 * Created by insonix on 25/11/16.
 */
public class AddServicePresenterImpl implements AddServicePresenter {

    AddServiceView mView;

    public AddServicePresenterImpl(AddServiceView mView) {
        this.mView = mView;
    }

    @Override
    public void addService(String tag, final String userid) {
        RequestArray requestArray = null;
        File dataFile;
        String compositeTag = "";
        if (mView.isInternetOk()) {
            if (tag.equalsIgnoreCase(CONTACTS_TAG)) {
                if(mView.askForPermissions()){
                    requestArray = mView.readContacts();
                }
            }
            if (tag.equalsIgnoreCase(CALL_TAG)) {
                if (mView.checkForPermissions()) {
                    requestArray = mView.readCallLogs();
                }
            }
            if (tag.equalsIgnoreCase(SMS_TAG)) {
                if(mView.checkForMsgPermission()){
                    requestArray = mView.readMsgs();
                }
            }
            ApiEndpointInterface apiService =
                    APIClient.getClient().create(ApiEndpointInterface.class);
            Call<GenericResponse> call;
            Gson gson = new Gson();
            String requestJson = gson.toJson(requestArray);
            Log.i("requestJson", "Details" + requestJson);
            if (!requestJson.equalsIgnoreCase("{\"dataArray\":[]}")) {
                dataFile = CommonUtils.createTxtFile(requestJson);
                // create RequestBody instance from file
                File datafile = new File(dataFile.getPath());
                Log.i("File", "size" + Integer.parseInt(String.valueOf(datafile.length() / 1024)));
                // create RequestBody instance from file
                RequestBody requestFile =
                        RequestBody.create(MediaType.parse("multipart/form-data"), dataFile);
                MultipartBody.Part body =
                        MultipartBody.Part.createFormData("file", dataFile.getName(), requestFile);
                RequestBody tagRequest =
                        RequestBody.create(
                                MediaType.parse("multipart/form-data"), tag);
                RequestBody phone =
                        RequestBody.create(
                                MediaType.parse("multipart/form-data"), userid);
                mView.onShowProgress();
                call = apiService.addService(tagRequest, phone, body);
                final String finalCompositeTag = compositeTag;
                call.enqueue(new Callback<GenericResponse>() {
                    @Override
                    public void onResponse(Call<GenericResponse> call, Response<GenericResponse> response) {
                        mView.onHideProgress();
                        Log.d("response","response"+response.raw());
                        Timber.i("Response" + response.body().getStatus());
                        if (response.body().getStatus().equalsIgnoreCase("true")) {
//                            mView.showSuccessMsg(R.string.sync_success);
                            mView.goToHome();
                        } else {
                            mView.showError(response.body().getMessage());
                        }
                    }

                    @Override
                    public void onFailure(Call<GenericResponse> call, Throwable t) {
                        mView.onHideProgress();
                        mView.showNotAvailable();
                        System.out.println(t.getMessage());
                    }
                });
            } else {
                mView.onHideProgress();
                mView.showNotAvailable();
                mView.showError("No Data Found.");
                Log.i("Tag", "addService:No data found ");
            }
        }
    }

    @Override
    public void updateService(String userid, SyncResponse.SycDataBean sycData) {
        if (mView.isInternetOk()) {
            mView.onShowProgress();
            ApiEndpointInterface apiService =
                    APIClient.getClient().create(ApiEndpointInterface.class);
            Call call = apiService.setSyncSettings(userid, sycData.getContactsync(), sycData.getSmssync(), sycData.getCallsync());
            call.enqueue(new Callback<GenericResponse>() {
                @Override
                public void onResponse(Call<GenericResponse> call, Response<GenericResponse> response) {
                    //                mView.onHideProgress();
                    Timber.i("Response" + response.body().getStatus());
                    mView.onHideProgress();
                    if (response.body().getStatus().equalsIgnoreCase("true")) {
                        mView.showSuccessMsg(R.string.success_msg);
                        mView.goToHome();
                    } else {
                        mView.showError("Unknown error occurred..please try again!!!");
                    }
                }

                @Override
                public void onFailure(Call<GenericResponse> call, Throwable t) {
                    mView.onHideProgress();
                    mView.showError(t.getMessage());
                }
            });
        } else {
            mView.showError("No internet connection available");
        }
    }

    @Override
    public void getSelectedService(String userid) {
        if (mView.isInternetOk()) {
            mView.onShowProgress();
            ApiEndpointInterface apiService =
                    APIClient.getClient().create(ApiEndpointInterface.class);
            Call call = apiService.getSyncSettings(userid);
            call.enqueue(new Callback<SyncResponse>() {
                @Override
                public void onResponse(Call<SyncResponse> call, Response<SyncResponse> response) {
                    mView.onHideProgress();
                    Log.d("response","response"+response.raw());
                    Timber.i("Response" + response.body().getStatus());
                    if (response.body().getStatus().equalsIgnoreCase("true")) {
                        mView.setViews(response.body().getSycData());
                        mView.hideSelectedViews(response.body().getSycData());
                    } else {
                        mView.showNotAvailable();
                        mView.showError("Unknown error occurred..please try again!!!");
                    }
                }

                @Override
                public void onFailure(Call<SyncResponse> call, Throwable t) {
                    mView.onHideProgress();
                    mView.showNotAvailable();
                }
            });
        } else {
            mView.showNotAvailable();
        }
    }

    @Override
    public void addAllServices(String phonenumber) {
        RequestArray requestArray;
        if (mView.askForPermissions()) {
            requestArray = mView.readContacts();
        }
        if (mView.checkForPermissions()) {
            requestArray = mView.readCallLogs();
        }
        if (mView.checkForMsgPermission()) {
            requestArray = mView.readMsgs();
        }

    }


}
