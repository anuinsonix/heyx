package com.insonix.heyx.presenter;

/**
 * Created by insonix on 1/12/16.
 */
public interface SettingsPresenter extends WebPresenter{


    void getSyncSettings(String userid);

    void updateSettings(String userid, String contactSyn, String msgSyn, String callSyn);
}
