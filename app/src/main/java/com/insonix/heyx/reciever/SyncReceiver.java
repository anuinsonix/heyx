package com.insonix.heyx.reciever;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.WakefulBroadcastReceiver;

import com.insonix.heyx.view.activity.NavigationActivity;


/**
 * Created by insonix on 25/11/16.
 * To receive notifications for app
 */

public class SyncReceiver extends WakefulBroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Intent intentTask = new Intent(context, NavigationActivity.class);
        intentTask.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intentTask);
    }
}
