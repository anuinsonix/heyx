package com.insonix.heyx.model;

/**
 * Created by insonix on 1/12/16.
 */

public class GenericResponse {
    /**
     * status : true
     * message : Call logs details uploaded successfully
     */

    private String status;
    private String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
