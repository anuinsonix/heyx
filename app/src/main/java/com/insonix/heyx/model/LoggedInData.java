
package com.insonix.heyx.model;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class LoggedInData {

    @SerializedName("city")
    private String mCity;
    @SerializedName("ip_address")
    private String mIpAddress;
    @SerializedName("last_login")
    private String mLastLogin;
    @SerializedName("locality")
    private String mLocality;
    @SerializedName("region")
    private String mRegion;
    @SerializedName("user_id")
    private String mUserId;

    public String getCity() {
        return mCity;
    }

    public void setCity(String city) {
        mCity = city;
    }

    public String getIpAddress() {
        return mIpAddress;
    }

    public void setIpAddress(String ip_address) {
        mIpAddress = ip_address;
    }

    public String getLastLogin() {
        return mLastLogin;
    }

    public void setLastLogin(String last_login) {
        mLastLogin = last_login;
    }

    public String getLocality() {
        return mLocality;
    }

    public void setLocality(String locality) {
        mLocality = locality;
    }

    public String getRegion() {
        return mRegion;
    }

    public void setRegion(String region) {
        mRegion = region;
    }

    public String getUserId() {
        return mUserId;
    }

    public void setUserId(String user_id) {
        mUserId = user_id;
    }

}
