package com.insonix.heyx.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by insonix on 7/12/16.
 */

public class NotificationBean implements Parcelable {
    /**
     * smssync : 1
     * contactsync : 0
     * callsync : 0
     */

    private String smssync;
    private String contactsync;
    private String callsync;

    public String getSmssync() {
        return smssync;
    }

    public void setSmssync(String smssync) {
        this.smssync = smssync;
    }

    public String getContactsync() {
        return contactsync;
    }

    public void setContactsync(String contactsync) {
        this.contactsync = contactsync;
    }

    public String getCallsync() {
        return callsync;
    }

    public void setCallsync(String callsync) {
        this.callsync = callsync;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.smssync);
        dest.writeString(this.contactsync);
        dest.writeString(this.callsync);
    }

    public NotificationBean(Parcel in) {
        this.smssync = in.readString();
        this.contactsync = in.readString();
        this.callsync = in.readString();
    }

    public static final Parcelable.Creator<NotificationBean> CREATOR = new Parcelable.Creator<NotificationBean>() {
        @Override
        public NotificationBean createFromParcel(Parcel source) {
            return new NotificationBean(source);
        }

        @Override
        public NotificationBean[] newArray(int size) {
            return new NotificationBean[size];
        }
    };
}
