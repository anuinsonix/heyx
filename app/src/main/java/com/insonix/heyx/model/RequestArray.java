package com.insonix.heyx.model;

import java.util.List;

/**
 * Created by insonix on 25/11/16.
 */

public class RequestArray {

    List<RequestData> dataArray;

    public List<RequestData> getDataArray() {
        return dataArray;
    }

    public void setDataArray(List<RequestData> dataArray) {
        this.dataArray = dataArray;
    }
}
