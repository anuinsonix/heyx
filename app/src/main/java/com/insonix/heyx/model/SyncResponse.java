package com.insonix.heyx.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by insonix on 1/12/16.
 */

public class SyncResponse implements Parcelable {
    /**
     * status : true
     * sycData : {"contactsync":"0","callsync":"0","smssync":"0"}
     */

    private String status;
    private SycDataBean sycData;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public SycDataBean getSycData() {
        return sycData;
    }

    public void setSycData(SycDataBean sycData) {
        this.sycData = sycData;
    }

    public static class SycDataBean implements Parcelable {
        /**
         * contactsync : 0
         * callsync : 0
         * smssync : 0
         */

        private String contactsync;
        private String callsync;
        private String smssync;

        public String getContactsync() {
            return contactsync;
        }

        public void setContactsync(String contactsync) {
            this.contactsync = contactsync;
        }

        public String getCallsync() {
            return callsync;
        }

        public void setCallsync(String callsync) {
            this.callsync = callsync;
        }

        public String getSmssync() {
            return smssync;
        }

        public void setSmssync(String smssync) {
            this.smssync = smssync;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeString(this.contactsync);
            dest.writeString(this.callsync);
            dest.writeString(this.smssync);
        }

        public SycDataBean() {
        }

        protected SycDataBean(Parcel in) {
            this.contactsync = in.readString();
            this.callsync = in.readString();
            this.smssync = in.readString();
        }

        public static final Creator<SycDataBean> CREATOR = new Creator<SycDataBean>() {
            @Override
            public SycDataBean createFromParcel(Parcel source) {
                return new SycDataBean(source);
            }

            @Override
            public SycDataBean[] newArray(int size) {
                return new SycDataBean[size];
            }
        };
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.status);
        dest.writeParcelable(this.sycData, flags);
    }

    public SyncResponse() {
    }

    protected SyncResponse(Parcel in) {
        this.status = in.readString();
        this.sycData = in.readParcelable(SycDataBean.class.getClassLoader());
    }

    public static final Parcelable.Creator<SyncResponse> CREATOR = new Parcelable.Creator<SyncResponse>() {
        @Override
        public SyncResponse createFromParcel(Parcel source) {
            return new SyncResponse(source);
        }

        @Override
        public SyncResponse[] newArray(int size) {
            return new SyncResponse[size];
        }
    };
}
