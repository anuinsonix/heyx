
package com.insonix.heyx.model;

import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class NotificationCheckEnableResponse {

    @SerializedName("noti_status")
    private String mnoti_status;
    @SerializedName("status")
    private String mStatus;

    public String getNotiStatus() {
        return mnoti_status;
    }

    public void setNotiStatus(String NotiStatus) {
        mnoti_status = NotiStatus;
    }

    public String getStatus() {
        return mStatus;
    }

    public void setStatus(String status) {
        mStatus = status;
    }

}
