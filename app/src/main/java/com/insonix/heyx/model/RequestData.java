package com.insonix.heyx.model;

/**
 * Created by insonix on 25/11/16.
 */

public class RequestData {
    /**
     * userNumber : 99887872656
     * callTime : 1480060176
     * userName : sunt aut facere repellat provident occaecati excepturi optio reprehenderit
     * smsdata : quia et suscipit
     * suscipit recusandae consequuntur expedita et
     */

    private String userNumber;
    private String callTime;
    private String userName;
    private String smsdata;
    private String call_duration;
    private String data_type;
    private String calltype;

    public String getMsg_type() {
        return msg_type;
    }

    public void setMsg_type(String msg_type) {
        this.msg_type = msg_type;
    }

    private String msg_type;


    public String getUserNumber() {
        return userNumber;
    }

    public void setUserNumber(String userNumber) {
        this.userNumber = userNumber;
    }

    public String getCallTime() {
        return callTime;
    }

    public void setCallTime(String callTime) {
        this.callTime = callTime;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getSmsdata() {
        return smsdata;
    }

    public void setSmsdata(String smsdata) {
        this.smsdata = smsdata;
    }

    public String getData_type() {
        return data_type;
    }

    public void setData_type(String data_type) {
        this.data_type = data_type;
    }


    public String getCalltype() {
        return calltype;
    }

    public void setCalltype(String calltype) {
        this.calltype = calltype;
    }

    public String getCall_duration() {
        return call_duration;
    }

    public void setCall_duration(String call_duration) {
        this.call_duration = call_duration;
    }
}
