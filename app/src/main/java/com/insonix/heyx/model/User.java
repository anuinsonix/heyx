package com.insonix.heyx.model;

/**
 * Created by insonix on 25/11/16.
 */

public class User {
    /**
     * message : Registered successfully
     * phonenumber : 87927222
     * status : true
     */

    private String message;
    private String phonenumber;
    private String status;
    private String userid;

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getPhonenumber() {
        return phonenumber;
    }

    public void setPhonenumber(String phonenumber) {
        this.phonenumber = phonenumber;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
