package com.insonix.heyx.utils;

import android.provider.Settings;

/**
 * Created by insonix on 25/11/16.
 */

public class Constants {

    public static final int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 111;
    public static final int MY_PERMISSIONS_REQUEST_READ_PHONE_STATE = 113;
    public static final int MY_PERMISSIONS_REQUEST_READ_CALL_LOG = 114;
    public static final int MY_PERMISSIONS_MULTIPLE_REQUEST=000;
    public static final int MY_PERMISSIONS_REQUEST_READ_SMS=115;
    public static final int MY_REQUESTSMS = 116;
    public static final String FCM_TOKEN="fcm_token";
    public static final String CONTACTS_TAG = "mycontact";
    public static final String SMS_TAG = "mysms";
    public static final String CALL_TAG = "mycalllog";

}
