package com.insonix.heyx.utils;

import android.app.Application;
import android.content.Context;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;

import java.util.UUID;

import timber.log.BuildConfig;
import timber.log.Timber;

import static java.security.AccessController.getContext;

/**
 * Created by insonix on 28/11/16.
 */

public class MyApplication extends Application {

    public static String OS="Android";
    private static MyApplication mInstance;
    public static MyPreferences mPrefs;
    public static Context context;

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }
        context = getApplicationContext();
    }


    public Context getContext(){
        return context;
    }


    @Override
    public void onLowMemory() {
        super.onLowMemory();
        Toast.makeText(getApplicationContext(),"Low memory",Toast.LENGTH_LONG).show();
    }

    public static MyApplication getInstance(){
            return mInstance;
        }

}
