package com.insonix.heyx.utils;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;

import com.bhargavms.dotloader.DotLoader;
import com.insonix.heyx.R;

/**
 * Created by insonix on 2/12/16.
 */

public class CommonProgress {

    public static Dialog dialog;

    public static void showProgress(Activity activity){
        dialog = new Dialog(activity, android.R.style.Theme_Translucent);
        View views = LayoutInflater.from(activity).inflate(R.layout.dot_dialog, null);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
//        dialog.getWindow().setBackgroundDrawableResource(R.color.feed_item_bg);
        dialog.setContentView(views);
        dialog.show();
    }

    public static void hideProgress() {
        if(dialog.isShowing())
        dialog.dismiss();
    }
}
