package com.insonix.heyx.utils;

import android.support.v4.app.Fragment;

/**
 * Created by insonix on 25/1/17.
 */

public interface BackPressListener {

    void onCustomBackPressed(Fragment fragment);
}
