package com.insonix.heyx.utils;

import android.support.v4.app.Fragment;

/**
 * Created by insonix on 3/1/17.
 */

public interface OnDialogClick {

    void openAddFragment(Fragment fragment);

}
