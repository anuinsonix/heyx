package com.insonix.heyx.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.insonix.heyx.model.User;

import timber.log.Timber;

import static com.insonix.heyx.utils.Constants.FCM_TOKEN;

/**
 * Created by insonix on 28/11/16.
 */

public class MyPreferences{

    public static final String SIGNDONE="signUp";
    SharedPreferences mPrefs;
    int MODE_PRIVATE=0;
    SharedPreferences.Editor editor;
    // Sharedpref file name
    private static final String sPREFNAME = "HeyxPrefs";

    public static final String sPHONENUMBER = "phonenumber";

    public static final String sSERVICESADDED = "services";
    public static final String sUSERNAME ="username";
    public static final String sUSERID ="userId";

    public MyPreferences(Context context) {
        Context mContext = context;
        mPrefs = mContext.getSharedPreferences(sPREFNAME, MODE_PRIVATE);
        editor = mPrefs.edit();
    }



    /**
     * Saving user details
     * @param name
     * @param phonenumber
     * @param userid
     */


    public void createLoginSession(String name, String phonenumber, String userid){
        // Storing name in pref
        editor.putString(sUSERNAME, name);
        // Storing email in pref
        editor.putString(sPHONENUMBER, phonenumber);
        editor.putString(sUSERID, userid);
        Timber.i("phone number in prefs"+phonenumber);
        Log.d("useriduserid","user_idpref"+userid);
        // commit changes
        editor.commit();
    }
    public String getValue(String key){

        return mPrefs.getString(key,"");
    }
    public User getLoginDetails(){
        User user=new User();
        user.setPhonenumber(mPrefs.getString(sPHONENUMBER,""));
        user.setUserid(mPrefs.getString(sUSERID,""));
        return user;
    }


    public void setToken(String token){
        editor.putString(FCM_TOKEN, token);
        editor.commit();
    }
    public String getToken(){

        return mPrefs.getString(FCM_TOKEN,"");

    }

    public String getsUSERNAME(){

        return mPrefs.getString(sUSERNAME,"");
    }

    public String setsUSERNAME(String name){

        return mPrefs.getString(sUSERNAME,name);
    }

    public void clear() {
        editor.clear();
        editor.commit();
    }

    public void clearName() {
        editor.putString(sUSERNAME, "");
        editor.commit();
    }


    public void setSignComplete(boolean b) {
        editor.putString(SIGNDONE, "");
        editor.commit();
    }
    public boolean getSignComplete() {
        return mPrefs.getBoolean(SIGNDONE,false);
    }
}
