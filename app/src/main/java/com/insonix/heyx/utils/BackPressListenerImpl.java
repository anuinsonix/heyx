package com.insonix.heyx.utils;

import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.insonix.heyx.view.fragment.AddServiceFragment;
import com.insonix.heyx.view.fragment.HomeFragment;

/**
 * Created by insonix on 25/1/17.
 */
public class BackPressListenerImpl implements BackPressListener {

    private final AppCompatActivity mActivity;
    private boolean doubleBackToExitPressedOnce=false;

    public BackPressListenerImpl(AppCompatActivity activity) {
        this.mActivity = activity;
    }

    @Override
    public void onCustomBackPressed(Fragment fragment) {
        if (fragment instanceof HomeFragment || fragment instanceof AddServiceFragment) {
            if (doubleBackToExitPressedOnce) {
                mActivity.onBackPressed();
                return;
            }
            this.doubleBackToExitPressedOnce = true;
            Toast.makeText(mActivity, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    doubleBackToExitPressedOnce = false;
                }
            }, 2000);
        } else {
            mActivity.getSupportFragmentManager().popBackStack();
//            mActivity.getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }
    }

}
