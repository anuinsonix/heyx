package com.insonix.heyx.utils;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;

import com.insonix.heyx.R;
import com.insonix.heyx.view.activity.AddedServiceActivity;
import com.insonix.heyx.view.fragment.AddServiceFragment;
import com.insonix.heyx.view.fragment.HomeFragment;
import com.insonix.heyx.view.fragment.SettingsFragment;

/**
 * Created by insonix on 3/1/17.
 */

public class MyAlertDialogFragment extends DialogFragment {

    public static MyAlertDialogFragment newInstance(String title, String msg, boolean status) {
        MyAlertDialogFragment frag = new MyAlertDialogFragment();
        Bundle args = new Bundle();
        args.putString("title", title);
        args.putString("msg",msg);
        args.putBoolean("status",status);
        frag.setArguments(args);
        return frag;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        String title = getArguments().getString("title");
        String msg = getArguments().getString("msg");
        final boolean stat=getArguments().getBoolean("status");
        return new AlertDialog.Builder(getActivity())
                .setTitle(title)
                .setMessage(msg)
                .setPositiveButton(R.string.alert_dialog_ok,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
//                                if(!stat){
//                                    AddServiceFragment frag = new AddServiceFragment();
//                                    openFragment(frag);
                                        startActivity(new Intent(getActivity(), AddedServiceActivity.class));
//                                    getActivity().finish();
                          /*      }else{
                                    HomeFragment frag=new HomeFragment();
                                    openFragment(frag);
                                }*/
                                dismiss();

                            }
                        }
                )
                .setNegativeButton(R.string.alert_dialog_cancel,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                dismiss();
                            }
                        }
                )
                .create();
    }

    private void openFragment(Fragment frag) {
        getActivity().invalidateOptionsMenu();
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.content_navigation, frag);
        ft.addToBackStack(String.valueOf(frag.getId()));
//        ft.addToBackStack(null);
        ft.setCustomAnimations(R.anim.slide_right, R.anim.slide_left);
        ft.commit();
    }
}
