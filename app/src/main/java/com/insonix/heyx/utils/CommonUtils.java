package com.insonix.heyx.utils;

import android.Manifest;
import android.app.Activity;
import android.app.KeyguardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;
import android.os.PowerManager;
import android.os.StrictMode;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.Window;
import android.widget.Toast;

import com.insonix.heyx.R;
import com.insonix.heyx.view.activity.NavigationActivity;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import static android.content.DialogInterface.BUTTON_POSITIVE;
import static com.insonix.heyx.utils.Constants.MY_PERMISSIONS_MULTIPLE_REQUEST;
import static com.insonix.heyx.utils.Constants.MY_PERMISSIONS_REQUEST_READ_CALL_LOG;
import static com.insonix.heyx.utils.Constants.MY_PERMISSIONS_REQUEST_READ_SMS;
import static io.fabric.sdk.android.Fabric.TAG;

/**
 * Created by insonix on 24/11/16.
 */

public class CommonUtils {


    private static String h;

    /**
     * To hide action bar when not needed
     *
     * @param activity
     */

    public static void hideActionBar(Activity activity) {
        activity.getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
    }

    /**
     * Normalize phone
     *
     * @param number
     * @return
     */

    public static String normalizePhoneNumber(Context ctx, String number) {

        number = number.replaceAll("[^+0-9]", ""); // All weird characters such as /, -, ...

        String country_code = ctx.getResources().getString(R.string.countrycode_de);

        if (number.substring(0, 1).compareTo("0") == 0 && number.substring(1, 2).compareTo("0") != 0 || number.substring(0, 2).compareTo("91") == 0 || number.substring(0, 3).compareTo("+91") == 0) {
            number = "+" + country_code + number.substring(1); // e.g. 0172 12 34 567 -> + (country_code) 172 12 34 567
        }

        number = number.replaceAll("^[0]{1,4}", "+"); // e.g. 004912345678 -> +4912345678
        Log.i(TAG, "normalizePhoneNumber: " + number);
        return number;
    }

    /**
     * Request permissions from user to read contacts
     */
    public static boolean askForPermissions(Activity activity) {
        boolean check_flag = true;
        String[] permissions = new String[]{
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_CONTACTS,
                Manifest.permission.READ_CONTACTS};

        // Here, thisActivity is the current activity

        for (String permission : permissions) {
            if (ContextCompat.checkSelfPermission(activity,
                    permission)
                    != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(activity,
                        permissions,
                        MY_PERMISSIONS_MULTIPLE_REQUEST);
                check_flag = false;
            }
        }
        return check_flag;
    }

    public static boolean getAllPermissions(Activity activity) {
        if (checkAndRequestPermissions(activity))
            if (checkMsgPermission(activity))
                if (checkCallLogPermission(activity)) {
                    return true;
                }
        return false;
    }


    //Multiple Permissions
    public static boolean checkAndRequestPermissions(Activity activity) {
        int permissionCAMERA = ContextCompat.checkSelfPermission(activity,
                Manifest.permission.READ_CONTACTS);


        int readStoragePermission = ContextCompat.checkSelfPermission(activity,

                Manifest.permission.READ_EXTERNAL_STORAGE);
        int writeStoragePermission = ContextCompat.checkSelfPermission(activity,

                Manifest.permission.WRITE_EXTERNAL_STORAGE);


        List<String> listPermissionsNeeded = new ArrayList<>();
        if (readStoragePermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        }
        if (writeStoragePermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (permissionCAMERA != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_CONTACTS);
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(activity,
                    listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), MY_PERMISSIONS_MULTIPLE_REQUEST);
            return false;
        }
        return true;
    }

    //Multiple Permissions
    public static boolean checkPermissions(String s, Activity activity) {
        boolean status = false;
        int permissionCONTACTS = ContextCompat.checkSelfPermission(activity,
                Manifest.permission.READ_CONTACTS);

        int readStoragePermission = ContextCompat.checkSelfPermission(activity,

                Manifest.permission.READ_EXTERNAL_STORAGE);
        int writeStoragePermission = ContextCompat.checkSelfPermission(activity,

                Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int smsReadPermission = ContextCompat.checkSelfPermission(activity,

                Manifest.permission.READ_SMS);
        int callLogReadPermission = ContextCompat.checkSelfPermission(activity,

                Manifest.permission.READ_CALL_LOG);


        List<String> listPermissionsNeeded = new ArrayList<>();
        if (readStoragePermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        }
        if (writeStoragePermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (permissionCONTACTS != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_CONTACTS);
        }
        if (smsReadPermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_SMS);
        }
        if (callLogReadPermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_CALL_LOG);
        }

        if (!s.contains(",") && !s.equalsIgnoreCase("")) {
            if (listPermissionsNeeded.contains(s)) {
                return status;
            }else{
                if(!s.equalsIgnoreCase("")){
                    status=true;
                    return status;
                }
            }
        }else if(s.equalsIgnoreCase("")){
            if (listPermissionsNeeded.size()==5 || listPermissionsNeeded.size()>0) {
                Log.i(TAG, "checkPermissions: "+status);
                return status;
            }else{
                status=true;
                return status;
            }
        } else {
            return splitnCheckPermissions(s, status, listPermissionsNeeded);

        }

        return status;
    }

    public static boolean splitnCheckPermissions(String s, boolean status, List<String> listPermissionsNeeded) {
        String[] permissions = s.split(",");
        ArrayList<Boolean> permissionsStatus = new ArrayList<>();
        for (int i = 0; i < permissions.length; i++) {
            if (listPermissionsNeeded.size()>0 && listPermissionsNeeded.contains(permissions[i])) {
                 permissionsStatus.add(true);           //true ,if permission needed
            }else{
                permissionsStatus.add(false);           //false,if permission  already granted
            }
        }

        //If single permission is false,then status will be returned false;
        if(areAllFalse(permissionsStatus)){
            status=true;
            return status;
        }
        else if(!areAllTrue(permissionsStatus)){
            return status;          //have some permissions already
        } else{
            return status;          //all permissions needed
        }
    }


    /**
     * Check if all items true in arrayList
     * @param array
     * @return
     */
    public static boolean areAllTrue(ArrayList<Boolean> array)
    {
        for(boolean b : array) if(!b) return false;
        return true;
    }


    /**
     * Check if all items false in arrayList
     * @param array
     * @return
     */
    public static boolean areAllFalse(ArrayList<Boolean> array)
    {
        for(boolean b : array) if(b) return false;
        return true;
    }


    /**
     * Request permissions from user to read contacts
     */
    public static void dummyPermissions(Activity activity) {
        String[] permissions = new String[]{
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_CONTACTS,
                Manifest.permission.READ_CONTACTS};

        // Here, thisActivity is the current activity

        for (String permission : permissions) {
            if (ContextCompat.checkSelfPermission(activity,
                    permission)
                    != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(activity,
                        new String[]{permission},
                        MY_PERMISSIONS_MULTIPLE_REQUEST);
            } else {
                Toast.makeText(activity, "Permission Denied", Toast.LENGTH_LONG).show();
            }
        }
    }


    /**
     * @param context
     * @return
     */


    public static boolean isInternetOn(Context context) {

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

        StrictMode.setThreadPolicy(policy);

        if (checkInternet()) {
            try {
                HttpURLConnection urlc = (HttpURLConnection) (new URL("http://heyx.insonix.com").openConnection());
                urlc.setRequestProperty("User-Agent", "Test");
                urlc.setRequestProperty("Connection", "close");
                urlc.setConnectTimeout(2000);
                urlc.connect();
                return (urlc.getResponseCode() == 200);
            } catch (Exception e) {
                Log.d("Internet", "isInternetOn: " + e);
            }
            return true;
        } else {
            showNoInternetDialog(context);
            return false;
        }
    }

    public static void showNoInternetDialog(Context context) {
        final AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setMessage("Internet not available..please try again later");
        alertDialog.setButton(BUTTON_POSITIVE, "Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
//                    context.fini();
                alertDialog.dismiss();
            }
        });

        alertDialog.show();
    }


    /**
     * To send feedback using email
     */

    public static void sendEmail() {
        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("message/rfc822");
        i.putExtra(Intent.EXTRA_EMAIL, new String[]{"insotes7876@gmail.com"});
        i.putExtra(Intent.EXTRA_SUBJECT, "Feedback");
        i.putExtra(Intent.EXTRA_TEXT, "Your feedback is important to us.Kindly share here.");
        try {
            MyApplication.getInstance().getContext().startActivity(Intent.createChooser(i, "Send mail...").addFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
        } catch (android.content.ActivityNotFoundException ex) {
            Log.i("Email", "Clients unavailable");
        }
    }


    /**
     * Checking permission for reading calls state
     */

    public static boolean checkCallLogPermission(Activity activity) {
        boolean permission_grant = true;

        // Here, thisActivity is the current activity
        String[] permissions = new String[]{
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.READ_CALL_LOG,
                Manifest.permission.WRITE_CALL_LOG};

        // Here, thisActivity is the current activity

        for (String permission : permissions) {
            if (ContextCompat.checkSelfPermission(activity,
                    permission)
                    != PackageManager.PERMISSION_GRANTED) {

                if (ActivityCompat.shouldShowRequestPermissionRationale(activity,
                        permission)) {

                    ActivityCompat.requestPermissions(activity,
                            permissions,
                            MY_PERMISSIONS_REQUEST_READ_CALL_LOG);

                } else {

                    ActivityCompat.requestPermissions(activity,
                            permissions,
                            MY_PERMISSIONS_REQUEST_READ_CALL_LOG);

                }
                permission_grant = false;
            }
        }
        return permission_grant;
    }


    /**
     * Checking permission for reading message
     */

    public static boolean checkMsgPermission(Activity activity) {
        // Here, thisActivity is the current activity
        boolean permission_grant = true;
        String[] permissions = new String[]{
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.READ_SMS};

        // Here, thisActivity is the current activity

        for (String permission : permissions) {
            if (ContextCompat.checkSelfPermission(activity,
                    permission)
                    != PackageManager.PERMISSION_GRANTED) {

                if (ActivityCompat.shouldShowRequestPermissionRationale(activity,
                        permission)) {

                    ActivityCompat.requestPermissions(activity,
                            permissions,
                            MY_PERMISSIONS_REQUEST_READ_SMS);

                } else {

                    ActivityCompat.requestPermissions(activity,
                            permissions,
                            MY_PERMISSIONS_REQUEST_READ_SMS);

                }
                permission_grant = false;
            }
        }
        return permission_grant;
    }


    /**
     * @param dataArray
     */

    public static File createTxtFile(String dataArray) {
        File filepath = null;  // file path to save

        try {
            h = DateFormat.format("MM-dd-yyyyy-h-mmssaa", System.currentTimeMillis()).toString();
            // this will create a new name everytime and unique
            File root = new File(Environment.getExternalStorageDirectory(), "Notes");
            // if external memory exists and folder with name Notes
            if (!root.exists()) {
                root.mkdirs(); // this will create folder.
            }

            filepath = new File(root, h + ".txt");
            FileWriter writer = new FileWriter(filepath);
            writer.append(dataArray);
            writer.flush();
            writer.close();
            String m = "File generated with name " + h + ".txt";
//            Toast.makeText(MyApplication.getInstance().getContext(), m, Toast.LENGTH_LONG).show();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return filepath;
    }


    public static void unlockDevice() {
        Context context = MyApplication.getInstance().getContext();
        KeyguardManager km = (KeyguardManager) context.getSystemService(Context.KEYGUARD_SERVICE);
        final KeyguardManager.KeyguardLock kl = km.newKeyguardLock("MyKeyguardLock");
        kl.disableKeyguard();

        PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        PowerManager.WakeLock wakeLock = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK
                | PowerManager.ACQUIRE_CAUSES_WAKEUP
                | PowerManager.ON_AFTER_RELEASE, "MyWakeLock");
        wakeLock.acquire();
    }


    public static boolean checkInternet() {
        ConnectivityManager conMgr = (ConnectivityManager) MyApplication.getInstance().getContext()
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo i = conMgr.getActiveNetworkInfo();
        if (i == null)
            return false;
        if (!i.isConnected())
            return false;
        return i.isAvailable();
    }

}
