package com.insonix.heyx.view.activity;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.insonix.heyx.R;
import com.insonix.heyx.model.RequestArray;
import com.insonix.heyx.model.RequestData;
import com.insonix.heyx.model.SyncResponse;
import com.insonix.heyx.presenter.AddServicePresenter;
import com.insonix.heyx.presenter.AddServicePresenterImpl;
import com.insonix.heyx.utils.CommonUtils;
import com.insonix.heyx.utils.MyPreferences;
import com.insonix.heyx.view.activity.AddServiceView;
import com.insonix.heyx.view.activity.BaseActivity;
import com.insonix.heyx.view.activity.NavigationActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.insonix.heyx.utils.CommonUtils.getAllPermissions;
import static com.insonix.heyx.utils.Constants.MY_PERMISSIONS_MULTIPLE_REQUEST;
import static com.insonix.heyx.utils.Constants.MY_PERMISSIONS_REQUEST_READ_CALL_LOG;
import static com.insonix.heyx.utils.Constants.MY_PERMISSIONS_REQUEST_READ_SMS;

/**
 * Created by insonix on 2/12/16.
 * This Activity is used to add services when add button is tapped on home or already added services screnn
 */

public class AddedServiceActivity extends BaseActivity implements AddServiceView {


    private AddServicePresenter mPresenter;
    @BindView(R.id.add_call_inner)
    LinearLayout addCallInner;
    @BindView(R.id.add_sms_inner)
    LinearLayout addSmsInner;
    @BindView(R.id.add_contacts_inner)
    LinearLayout addContactsInner;
    @BindView(R.id.selected_img)
    ImageView selectedImg;
    @BindView(R.id.selected_img1)
    ImageView selectedImg1;
    @BindView(R.id.selected_img2)
    ImageView selectedImg2;
    @BindView(R.id.add_call)
    FrameLayout addCall;
    @BindView(R.id.add_sms)
    FrameLayout addSms;
    @BindView(R.id.add_contacts)
    FrameLayout addContacts;
    @BindView(R.id.no_response)
    TextView noResponse;
    @BindView(R.id.not_available)
    LinearLayout notAvailable;
    @BindView(R.id.retry_view)
    ImageView retryView;
    @BindView(R.id.parent)
    LinearLayout parent;
    @BindView(R.id.parent_scroll)
    ScrollView parentScroll;
    private static int choice = 7;
    private MyPreferences mPrefs;
    RequestArray requestArray = new RequestArray();
    List<RequestData> fetchedData = new ArrayList<>();
    private SyncResponse.SycDataBean syncBean;
    MenuItem item;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_service_view_fragment);
        ButterKnife.bind(this);
        setTitle(getResources().getString(R.string.add_services));
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        mPresenter = new AddServicePresenterImpl(this);
        mPrefs = new MyPreferences(this);
        if (CommonUtils.checkInternet()) {
            mPresenter.getSelectedService(mPrefs.getLoginDetails().getUserid());
        }
        invalidateOptionsMenu();
    }

    @OnClick({R.id.add_call_inner, R.id.add_sms_inner, R.id.add_contacts_inner})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.add_call_inner:
                if (selectedImg.getVisibility() == View.GONE) {
                    selectedImg.setVisibility(View.VISIBLE);
                    if (selectedImg1.getVisibility() == View.GONE && selectedImg2.getVisibility() == View.GONE) {
                        choice = 0;
                    } else {
                        choice = getChoiceValue();
                    }
                } else {
                    selectedImg.setVisibility(View.GONE);
                    if (selectedImg1.getVisibility() == View.GONE && selectedImg2.getVisibility() == View.GONE) {
                        choice = 7;
                    } else {
                        choice = getChoiceValue();
                    }
                }
                getDoneVisible();
                break;
            case R.id.add_sms_inner:
                if (selectedImg1.getVisibility() == View.GONE) {
//                    checkForMsgPermission();
                    selectedImg1.setVisibility(View.VISIBLE);
                    if (selectedImg.getVisibility() == View.GONE && selectedImg2.getVisibility() == View.GONE) {
                        choice = 1;
                    } else {
                        choice = getChoiceValue();
                    }
                } else {
                    selectedImg1.setVisibility(View.GONE);
                    if (selectedImg.getVisibility() == View.GONE && selectedImg2.getVisibility() == View.GONE) {
                        choice = 7;
                    } else {
                        choice = getChoiceValue();
                    }
                }
                getDoneVisible();
                break;
            case R.id.add_contacts_inner:
                if (selectedImg2.getVisibility() == View.GONE) {
//                    askForPermissions();
                    selectedImg2.setVisibility(View.VISIBLE);
                    if (selectedImg.getVisibility() == View.GONE && selectedImg1.getVisibility() == View.GONE) {
                        choice = 2;
                    } else {
                        choice = getChoiceValue();
                    }
                } else {
                    selectedImg2.setVisibility(View.GONE);
                    if (selectedImg1.getVisibility() == View.GONE && selectedImg.getVisibility() == View.GONE) {
                        choice = 7;
                    } else {
                        choice = getChoiceValue();
                    }
                }
                getDoneVisible();
                break;
            default:
                getDoneVisible();
                choice = getChoiceValue();
        }
    }

    private void getDoneVisible() {
        if((selectedImg.getVisibility()==View.VISIBLE)||(selectedImg1.getVisibility()==View.VISIBLE)||(selectedImg2.getVisibility()==View.VISIBLE)){
            item.setVisible(true);
        }else{
            item.setVisible(false);
        }
    }

    private int getChoiceValue() {
        if (selectedImg.getVisibility() == View.VISIBLE && selectedImg1.getVisibility() == View.VISIBLE && selectedImg2.getVisibility() == View.VISIBLE) {
            choice = 3;
        } else if (selectedImg.getVisibility() == View.VISIBLE && selectedImg1.getVisibility() == View.VISIBLE) {
            choice = 4;
        } else if (selectedImg.getVisibility() == View.VISIBLE && selectedImg2.getVisibility() == View.VISIBLE) {
            choice = 5;
        } else if (selectedImg1.getVisibility() == View.VISIBLE && selectedImg2.getVisibility() == View.VISIBLE) {
            choice = 6;
        } else {
            choice = 7;
        }
        return choice;
    }


    @Override
    public void onShowProgress() {
        super.onShowProgress();
    }

    @Override
    public void onHideProgress() {
        super.onHideProgress();
    }

    @Override
    public boolean askForPermissions() {
        return CommonUtils.askForPermissions(this);
    }

    @Override
    public boolean checkForPermissions() {
        return CommonUtils.checkCallLogPermission(this);
    }

    @Override
    public boolean checkForMsgPermission() {
        return CommonUtils.checkMsgPermission(this);
    }

    @Override
    public void hideSelectedViews(SyncResponse.SycDataBean sycData) {
        if (sycData.getSmssync().equalsIgnoreCase("0")) {
            addSms.setVisibility(View.VISIBLE);
        }
        if (sycData.getContactsync().equalsIgnoreCase("0")) {
            addContacts.setVisibility(View.VISIBLE);
        }
        if (sycData.getCallsync().equalsIgnoreCase("0")) {
            addCall.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public RequestArray readContacts() {
        return new RequestArray();
    }

    @Override
    public RequestArray readMsgs() {
        return new RequestArray();
    }

    @Override
    public RequestArray readCallLogs() {
        return new RequestArray();
    }

    @Override
    public void showError(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
    }

    @Override
    public void setViews(SyncResponse.SycDataBean sycData) {
        retryView.setVisibility(View.GONE);
        notAvailable.setVisibility(View.GONE);
        syncBean = sycData;
    }

    @Override
    public void goToHome() {
        startActivity(new Intent(this, NavigationActivity.class));
        overridePendingTransition(R.anim.slide_right, R.anim.slide_left);
        finish();
    }

    @Override
    public void showNotAvailable() {
        parentScroll.setVisibility(View.GONE);
        notAvailable.setVisibility(View.VISIBLE);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.addservices, menu);
        item = menu.findItem(R.id.done_button);
        item.setVisible(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.done_button) {
            clickEvent(choice);
        }

        return super.onOptionsItemSelected(item);
    }

    private void clickEvent(int choice) {
        switch (choice) {
            case 0:
//                    sync calls only
                if (checkForPermissions()) {
                    syncBean.setCallsync("1");
                    mPresenter.updateService(mPrefs.getLoginDetails().getUserid(), syncBean);
                }
                break;
            case 1:
//                    sync msgs only
                if (checkForMsgPermission()) {
                    syncBean.setSmssync("1");
                    mPresenter.updateService(mPrefs.getLoginDetails().getUserid(), syncBean);
                }
                break;
            case 2:
                //                    sync contacts only
                boolean var = CommonUtils.checkAndRequestPermissions(this);
               // Log.i(TAG, "clickEvent: " + var);
                if (var == true) {
                    addContacts();
                }
                break;
            case 3:
                //   sync contacts,msgs and calls
                if (getAllPermissions(this)) {
                    syncBean.setSmssync("1");
                    syncBean.setContactsync("1");
                    syncBean.setCallsync("1");
                    mPresenter.updateService(mPrefs.getLoginDetails().getUserid(), syncBean);
                }
                break;
            case 4:
                //                    sync calls,msgs only
                if (checkForMsgPermission()) {
                    if (checkForPermissions()) {
                        syncBean.setSmssync("1");
                        syncBean.setCallsync("1");
                        mPresenter.updateService(mPrefs.getLoginDetails().getUserid(), syncBean);
                    }
                }
                break;
            case 5:
                //                    sync calls and contacts only
                if (askForPermissions()) {
                    if (checkForPermissions()) {
                        syncBean.setContactsync("1");
                        syncBean.setCallsync("1");
                        mPresenter.updateService(mPrefs.getLoginDetails().getUserid(), syncBean);
                    }
                }

                break;
            case 6:
                //                    sync contacts and msgs only
                if (checkForMsgPermission())
                    if (CommonUtils.checkAndRequestPermissions(this)) {
                        syncBean.setContactsync("1");
                        syncBean.setSmssync("1");
                        mPresenter.updateService(mPrefs.getLoginDetails().getUserid(), syncBean);
                    }
                break;

            case 7:
                syncBean.setContactsync("0");
                syncBean.setSmssync("0");
                syncBean.setCallsync("0");
                mPresenter.updateService(mPrefs.getLoginDetails().getUserid(), syncBean);
                break;
        }
    }


    private void addContacts() {
        syncBean.setContactsync("1");
        mPresenter.updateService(mPrefs.getLoginDetails().getUserid(), syncBean);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_MULTIPLE_REQUEST:
                if (grantResults.length > 0 && checkIfallGranted(grantResults)) {
                    clickEvent(choice);
                }
                break;
            case MY_PERMISSIONS_REQUEST_READ_CALL_LOG:
                if (grantResults.length > 0 && checkIfallGranted(grantResults)) {
                    clickEvent(choice);
                }
                break;
            case MY_PERMISSIONS_REQUEST_READ_SMS:
                if (grantResults.length > 0 && checkIfallGranted(grantResults)) {
                    clickEvent(choice);
                }
                break;
        }
    }

    private boolean checkIfallGranted(int[] grantResult) {

        for (int aGrantResult : grantResult) {
         //   Log.i(TAG, "checkIfallGranted:" + grantResult.length);
            if (aGrantResult != PackageManager.PERMISSION_GRANTED) {
                return false;
            }
        }
      //  Log.i(TAG, "checkIfallGranted: true");
        return true;
    }

    @Override
    public boolean isInternetOk() {
        return CommonUtils.checkInternet();
    }

    @OnClick(R.id.retry_view)
    public void onClick() {
        mPresenter.getSelectedService(mPrefs.getLoginDetails().getUserid());

    }

}
