package com.insonix.heyx.view.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.insonix.heyx.R;
import com.insonix.heyx.utils.MyPreferences;

import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.insonix.heyx.utils.CommonProgress.hideProgress;
import static com.insonix.heyx.utils.CommonProgress.showProgress;

/**
 * Created by root on 27/9/17.
 */

public class PhoneNumberActivity extends BaseActivity {

    private static final String TAG = "PhoneNumberActivity";
    @BindView(R.id.input_Phone)
    EditText inputPhone;
    @BindView(R.id.input_layout_phone_no)
    TextInputLayout inputLayoutPhoneNo;
    @BindView(R.id.text_phn)
    TextView textPhn;
    @BindView(R.id.btn_submit)
    Button btnSubmit;
    @BindView(R.id.linear_phn)
    RelativeLayout linearPhn;
    @BindView(R.id.text_otp)
    TextView textOtp;
    @BindView(R.id.wrong_number)
    TextView wrong_number;
    @BindView(R.id.edit_otp)
    EditText editOtp;
    @BindView(R.id.btn_resend)
    Button btnResend;
    @BindView(R.id.btn_verifyotp)
    Button btnVerifyotp;
    @BindView(R.id.linear_otp)
    LinearLayout linearOtp;

    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks;
    private PhoneAuthCredential credential1;
    private FirebaseAuth mAuth;
    private String verifyId1;
    private MyPreferences mMyPrefs;
    String verificationcode;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phone_auth);
        ButterKnife.bind(this);
        mMyPrefs=new MyPreferences(this);
        mAuth = FirebaseAuth.getInstance();


        getPhnVerify();


    }




    @OnClick({R.id.btn_submit, R.id.btn_resend, R.id.wrong_number, R.id.btn_verifyotp})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_submit:

                if (!TextUtils.isEmpty(inputPhone.getText().toString())) {
                    mMyPrefs.createLoginSession("",inputPhone.getText().toString().trim(),"");
                   // StoreData.saveTokenValue(PhoneNumberActivity.this, Singleton.keys.MOBILE.name(), inputPhone.getText().toString());

                  //  TelephonyManager tm = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);  //gets the current TelephonyManager
                  //  if (tm.getSimState() != TelephonyManager.SIM_STATE_ABSENT){

                    showProgress(PhoneNumberActivity.this);
                        // Singleton.getinstance().showProgressDialog("Loading",ActivityPhoneAuth.this);
                        PhoneAuthProvider.getInstance().verifyPhoneNumber(inputPhone.getText().toString().trim(), 60, TimeUnit.MICROSECONDS, PhoneNumberActivity.this, mCallbacks);
                        textOtp.setText(getString(R.string.otp_text, inputPhone.getText().toString()));

                   // }




                } else {
                    inputPhone.setError(getString(R.string.err_phn_number));
                }
                break;
            case R.id.btn_resend:
                btnResend.setEnabled(false);
                btnResend.setTextColor(getResources().getColor(R.color.colorAccent));
                PhoneAuthProvider.getInstance().verifyPhoneNumber(mMyPrefs.getLoginDetails().getPhonenumber(), 60, TimeUnit.MICROSECONDS, PhoneNumberActivity.this, mCallbacks);

                break;
            case R.id.btn_verifyotp:
               verifyuser(verificationcode);
                break;
            case R.id.wrong_number:
                startActivity(new Intent(this, PhoneNumberActivity.class));
                finish();
                break;
        }
    }


    public void getPhnVerify() {
        mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {

            @Override
            public void onVerificationCompleted(PhoneAuthCredential credential) {
                credential1 = credential;
                mAuth.signInWithCredential(credential)
                        .addOnCompleteListener(PhoneNumberActivity.this, new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                hideProgress();
                                //  Singleton.getinstance().closeProgressDialog();
                                if (task.isSuccessful()) {
                                    FirebaseUser user = task.getResult().getUser();
                               //   StoreData.saveTokenValue(PhoneNumberActivity.this, Singleton.keys.MOBILE.name(), user.getPhoneNumber());
                                    mMyPrefs.createLoginSession("",user.getPhoneNumber()+"", "");
                                    Intent intent = new Intent(PhoneNumberActivity.this, SignUpActivity.class);
                                    startActivity(intent);
                                    finish();
                                    // Singleton.getinstance().saveValue(NumberAuthenticationActivity.this, PHONENUMBER, user.getPhoneNumber());
                                    //   jsonotp(android_id, user.getPhoneNumber());

                                } else {
                                    // Sign in failed, display a message and update the UI
                                    if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                                        // The verification code entered was invalid

                                    }
                                }
                            }
                        });

            }

            @Override
            public void onVerificationFailed(FirebaseException e) {
                //  Singleton.getinstance().closeProgressDialog();
                hideProgress();

               // Toast.makeText(getApplicationContext(),e.getMessage(),Toast.LENGTH_LONG).show();
                if (e.getMessage().contains("E.164 format")){
                    if (inputPhone != null) {
                        inputPhone.requestFocus();
                        inputPhone.setError(getString(R.string.err_phn_number_format));
                    }
                }else{
                    inputPhone.requestFocus();
                    inputPhone.setError(e.getMessage());
                }
            }

            @Override
            public void onCodeSent(String verifyId, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
                super.onCodeSent(verifyId, forceResendingToken);
                verifyId1 = verifyId;
                linearOtp.setVisibility(View.VISIBLE);
                linearPhn.setVisibility(View.GONE);
                // Singleton.getinstance().closeProgressDialog();
                hideProgress();


                textWatcher();
            }

            @Override
            public void onCodeAutoRetrievalTimeOut(String s) {
                super.onCodeAutoRetrievalTimeOut(s);
                hideProgress();

                //  Singleton.getinstance().closeProgressDialog();
                if (btnResend != null) {
                    btnResend.setEnabled(true);
                    btnResend.setTextColor(getResources().getColor(R.color.colorWhiter));
                }

            }
        };


    }


    void textWatcher() {

        editOtp.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() > 0) {
                    Log.d(TAG, "afterTextChanged: " + editable);
                    if (editable.length() == 6 && !TextUtils.isEmpty(verifyId1)) {
                        verificationcode=editable.toString();
                        btnVerifyotp.setEnabled(true);
                        btnVerifyotp.animate();



                    }

                }
            }
        });


    }
    public void verifyuser(String editable){
        showProgress(PhoneNumberActivity.this);
        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(verifyId1, editable);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(PhoneNumberActivity.this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            hideProgress();
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithCredential:success");

                            FirebaseUser user = task.getResult().getUser();
                            Log.d(TAG, "onComplete: " + user.getPhoneNumber());
                            Intent intent = new Intent(PhoneNumberActivity.this, SignUpActivity.class);
                            startActivity(intent);
                            finish();
                            mMyPrefs.createLoginSession("",user.getPhoneNumber()+"", "");

                            //    jsonotp(android_id, user.getPhoneNumber());
                            //    Singleton.getinstance().saveValue(ActivityPhoneAuth.this, PHONENUMBER, user.getPhoneNumber());
                        } else {
                            hideProgress();

                            // Sign in failed, display a message and update the UI
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                            if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                                // The verification code entered was invalid
                                if (editOtp != null) {
                                    editOtp.requestFocus();
                                    editOtp.setError(getString(R.string.err_otp));

                                }
                            }
                        }
                    }
                });
    }



}
