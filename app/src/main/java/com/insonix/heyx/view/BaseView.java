package com.insonix.heyx.view;

/**
 * Created by root on 27/9/17.
 */

public interface BaseView {
    void showProgress();

    void hideProgress();

    void showError(String msg);
}
