package com.insonix.heyx.view.activity;

import com.insonix.heyx.model.User;

/**
 * Created by insonix on 25/11/16.
 */
public interface SignUpView extends Baseview{


    void showNameError();

    void showPasswordError();

    void showConfirmPasswordError();

    void showPasswordLengthError();

    void showPasswordMismatchError();

    void moveToNext(User body);

    void showError();

    void setProfile(String username);
}
