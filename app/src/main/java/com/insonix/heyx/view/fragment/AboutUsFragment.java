package com.insonix.heyx.view.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.insonix.heyx.R;
import com.insonix.heyx.utils.BackPressListenerImpl;
import com.insonix.heyx.utils.CommonUtils;
import com.insonix.heyx.view.activity.BaseActivity;

/**
 * Created by insonix on 1/12/16.
 */

public class AboutUsFragment extends BaseFragment {

    private FragmentActivity activity;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_aboutus,container,false);
        getActivity().setTitle(R.string.about_us_title);
        activity = getActivity();
        ((BaseActivity)activity).setOnBackPressedListener(new BackPressListenerImpl((AppCompatActivity) activity));
        return view;
    }

    @Override
    public boolean isInternetOk() {
        return CommonUtils.checkInternet();
    }
}
