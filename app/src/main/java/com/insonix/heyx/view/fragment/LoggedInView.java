package com.insonix.heyx.view.fragment;

import com.insonix.heyx.model.LoggedBean;
import com.insonix.heyx.view.activity.Baseview;

/**
 * Created by insonix on 19/12/16.
 */
public interface LoggedInView extends Baseview{

    void showLastLoggedInSessions(LoggedBean logData);

    void showUnavailable();
}
