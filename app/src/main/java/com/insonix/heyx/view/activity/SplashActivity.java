package com.insonix.heyx.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceIdService;
import com.insonix.heyx.R;
import com.insonix.heyx.presenter.SplashPresenter;
import com.insonix.heyx.presenter.SplashPresenterImpl;
import com.insonix.heyx.utils.MyApplication;
import com.insonix.heyx.utils.MyPreferences;


import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.R.attr.phoneNumber;
import static com.insonix.heyx.utils.MyApplication.mPrefs;

/**
 * Created by insonix on 24/11/16.
 */

public class SplashActivity extends AppCompatActivity {

    // Note: Your consumer key and secret should be obfuscated in your source code before shipping.
    private static final String TWITTER_KEY = "E9XayYD85IFImmL1XfeV4Auri";
    private static final String TWITTER_SECRET = "TBz8zc3AzoB1SGHBM3K4CKcdb7dxwWHFJ66vKB5BVIRDKQau9r";


    SplashPresenter mPresenter;
    // @BindView(R.id.auth_button)

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //TwitterAuthConfig authConfig = new TwitterAuthConfig(TWITTER_KEY, TWITTER_SECRET);
        // Fabric.with(this, new TwitterCore(authConfig), new Digits.Builder().build());
      //  this.getSupportActionBar().hide();//        CommonUtils.hideActionBar(this);
        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);
        // mPresenter = new SplashPresenterImpl(this);
        //   mPresenter.runWaitThread(2000);
        final MyPreferences mMyPrefs=new MyPreferences(this);
//        authButton.setAuthTheme(R.style.ButtonTheme);
//        authButton.setCallback(new AuthCallback() {
//            @Override
//            public void success(DigitsSession session, String phoneNumber) {
//                if(mMyPrefs.getLoginDetails().getPhonenumber().isEmpty()){
//                    mMyPrefs.createLoginSession("",phoneNumber.replaceAll("[-+.^:,]",""));
//                    startActivity(new Intent(SplashActivity.this, SignUpActivity.class));
//                    finish();
//                }else{
//                    startActivity(new Intent(SplashActivity.this, NavigationActivity.class));
//                    finish();
//                }
//
//            }
//
//            @Override
//            public void failure(DigitsException exception) {
//                Log.d("Digits", "Sign in with Digits failure", exception);
//            }
//        });


        Thread timer= new Thread()
        {
            public void run()
            {
                try
                {
                    //Display for 3 seconds
                    sleep(3000);
                }
                catch (InterruptedException e)
                {
                    // TODO: handle exception
                    e.printStackTrace();
                }
                finally
                {
                    //Goes to Activity  StartingPoint.java(STARTINGPOINT)
                    if(mMyPrefs.getLoginDetails().getPhonenumber().isEmpty()){
                        // mMyPrefs.createLoginSession("",phoneNumber.replaceAll("[-+.^:,]",""));
                        startActivity(new Intent(SplashActivity.this, PhoneNumberActivity.class));
                        finish();
                    }else if (mMyPrefs.getValue(MyPreferences.sUSERNAME).isEmpty()){
                        startActivity(new Intent(SplashActivity.this, SignUpActivity.class));
                        finish();
                    }else {
                        startActivity(new Intent(SplashActivity.this, NavigationActivity.class));
                        finish();
                    }
                }
            }
        };
        timer.start();
    }


    //Destroy Welcome_screen.java after it goes to next activity
    @Override
    protected void onPause()
    {
        // TODO Auto-generated method stub
        super.onPause();
        finish();

    }

}
