package com.insonix.heyx.view.activity;

import com.insonix.heyx.model.SyncResponse;

/**
 * Created by insonix on 24/11/16.
 */
public interface Homeview extends Baseview {

    void changeView(SyncResponse.SycDataBean sycData);

    void showUnavailable();

}
