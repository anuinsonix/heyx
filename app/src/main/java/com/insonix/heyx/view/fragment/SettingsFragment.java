package com.insonix.heyx.view.fragment;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;

import com.insonix.heyx.R;
import com.insonix.heyx.model.SyncResponse;
import com.insonix.heyx.presenter.SettingsPresenter;
import com.insonix.heyx.presenter.SettingsPresenterImpl;
import com.insonix.heyx.utils.BackPressListenerImpl;
import com.insonix.heyx.utils.CommonUtils;
import com.insonix.heyx.utils.MyAlertDialogFragment;
import com.insonix.heyx.utils.MyPreferences;
import com.insonix.heyx.utils.OnDialogClick;
import com.insonix.heyx.view.activity.BaseActivity;
import com.insonix.heyx.view.activity.NavigationActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.insonix.heyx.utils.Constants.MY_PERMISSIONS_MULTIPLE_REQUEST;
import static com.insonix.heyx.utils.Constants.MY_PERMISSIONS_REQUEST_READ_CALL_LOG;
import static com.insonix.heyx.utils.Constants.MY_PERMISSIONS_REQUEST_READ_SMS;

/**
 * Created by prashant on 29/11/16.
 */

public class SettingsFragment extends BaseFragment implements SettingsView, OnDialogClick {

    SettingsPresenter mPresenter;
    String userid;
    MyPreferences mMyPreferences;
    @BindView(R.id.textView1)
    TextView textView1;
    @BindView(R.id.textView2)
    TextView textView2;
    @BindView(R.id.textView3)
    TextView textView3;
    @BindView(R.id.call_switch)
    SwitchCompat callSwitch;
    @BindView(R.id.sms_switch)
    SwitchCompat smsSwitch;
    @BindView(R.id.contact_switch)
    SwitchCompat contactSwitch;
    private String contactSyn = "0", callSyn = "0", msgSyn = "0";
    private int choice;
    private FragmentActivity activity;
    boolean status = false;                 //status variable is sent to alertdialog class to differntiate which fragment to open:HOme or added services


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.settings_fragment, container, false);
        mMyPreferences = new MyPreferences(getActivity());
        userid = mMyPreferences.getLoginDetails().getUserid();
        ButterKnife.bind(this, view);
        getActivity().setTitle(R.string.settings_title);
        setHasOptionsMenu(true);
       // Log.i(TAG, "onCreateView: " + status);
        mPresenter = new SettingsPresenterImpl(this);
        mPresenter.getSyncSettings(userid);
        activity = getActivity();
        ((BaseActivity) activity).setOnBackPressedListener(new BackPressListenerImpl((AppCompatActivity) activity));
        contactSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    contactSyn = "1";
                } else {
                    contactSyn = "0";
                }
            }
        });
        smsSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    msgSyn = "1";
                } else {
                    msgSyn = "0";
                }
            }
        });
        callSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    callSyn = "1";
                } else {
                    callSyn = "0";
                }
            }
        });
        return view;
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        getActivity().getMenuInflater().inflate(R.menu.addservices, menu);
    }

    /**
     * To get selected choices
     *
     * @return
     */


    private int getChoice() {
        choice = 7;
        if (contactSyn.equalsIgnoreCase("1")) {
            choice = 0;
        }
        if (callSyn.equalsIgnoreCase("1")) {
            choice = 1;
        }
        if (msgSyn.equalsIgnoreCase("1")) {
            choice = 2;
        }
        if (contactSyn.equalsIgnoreCase("1") && callSyn.equalsIgnoreCase("1")) {
            choice = 3;
        }
        if (callSyn.equalsIgnoreCase("1") && msgSyn.equalsIgnoreCase("1")) {
            choice = 4;
        }
        if (msgSyn.equalsIgnoreCase("1") && contactSyn.equalsIgnoreCase("1")) {
            choice = 5;
        }
        if (msgSyn.equalsIgnoreCase("1") && contactSyn.equalsIgnoreCase("1") && callSyn.equalsIgnoreCase("1")) {
            choice = 6;
        }
        return choice;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.done_button) {
           // Log.i(TAG, "onOptionsItemSelected: " + getChoice());
            clickEvent(getChoice());
        }
        return super.onOptionsItemSelected(item);
    }


    private void clickEvent(int choice) {
        switch (choice) {
            case 0:
//                status = true;
                if (CommonUtils.checkPermissions("android.permission.READ_CONTACTS", getActivity())) {
//                mPresenter.updateSettings(phone, contactSwitch.isChecked() ? "1" : "0", smsSwitch.isChecked() ? "1" : "0", callSwitch.isChecked() ? "1" : "0");
                    mPresenter.updateSettings(userid, "1", "0", "0");

                } else {
                    showErrorDialog("Contacts Permission missing");
                }
                break;
            case 1:
//                status = true;
                if (CommonUtils.checkPermissions("android.permission.READ_CALL_LOG", getActivity())) {
//                mPresenter.updateSettings(phone, contactSwitch.isChecked() ? "1" : "0", smsSwitch.isChecked() ? "1" : "0", callSwitch.isChecked() ? "1" : "0");
                    mPresenter.updateSettings(userid, "0", "0", "1");

                } else {
                    showErrorDialog("Call logs Permission missing");
                }
                break;
            case 2:
//                status = true;
                if (CommonUtils.checkPermissions("android.permission.READ_SMS", getActivity())) {
//                mPresenter.updateSettings(phone, contactSwitch.isChecked() ? "1" : "0", smsSwitch.isChecked() ? "1" : "0", callSwitch.isChecked() ? "1" : "0");
                    mPresenter.updateSettings(userid, "0", "1", "0");

                } else {
                    showErrorDialog("Messages Permission missing");
                }
                break;
            case 3:

                // status will be set to false,if both permissions missing
//                status = true;
                if (CommonUtils.checkPermissions("android.permission.READ_CONTACTS,android.permission.READ_CALL_LOG", getActivity())) {
                    mPresenter.updateSettings(userid, "1", "0", "1");

                } else {
                    // status will be set to true,if any of the permissions is available
                  /*  if (CommonUtils.checkPermissions("android.permission.READ_CONTACTS", getActivity()) || CommonUtils.checkPermissions("android.permission.READ_CALL_LOG", getActivity())) {
                        status = false;
                        showErrorDialog("Call Logs and Contacts Permission missing");
                    } else {*/
                        showErrorDialog("Call Logs and Contacts Permission missing");
//                    }
                }
                break;
            case 4:
//                status = true;
                if (CommonUtils.checkPermissions("android.permission.READ_SMS,android.permission.READ_CALL_LOG", getActivity())) {
                    mPresenter.updateSettings(userid, "0", "1", "1");

                } else {
                    // status will be set to true,if any of the permissions is available
                   /* if (CommonUtils.checkPermissions("android.permission.READ_SMS", getActivity()) || CommonUtils.checkPermissions("android.permission.READ_CALL_LOG", getActivity())) {
                        status = false;
                        showErrorDialog("Messages and Call logs Permission missing");
                    } else {*/
                        showErrorDialog("Messages and Call logs Permission missing");
//                    }
                }
                break;
            case 5:
//                status = true;
                if (CommonUtils.checkPermissions("android.permission.READ_CONTACTS,android.permission.READ_SMS", getActivity())) {
                    mPresenter.updateSettings(userid, "1", "1", "0");
                } else {
                    // status will be set to true,if any of the permissions is available
                    if (CommonUtils.checkPermissions("android.permission.READ_CONTACTS", getActivity()) || CommonUtils.checkPermissions("android.permission.READ_SMS", getActivity())) {
                       /* if (CommonUtils.checkPermissions("android.permission.READ_CALL_LOG", getActivity())) {
                            status = false;
                            showErrorDialog("Contacts and Messages Permission missing");
                        }

                    } else {*/
                        showErrorDialog("Contacts and Messages Permission missing");
                    }
                }
                break;
            case 6:
                if (CommonUtils.checkPermissions("", getActivity())) {
                    mPresenter.updateSettings(userid, "1", "1", "1");
                } else {
                    /*if (CommonUtils.checkPermissions("android.permission.READ_CONTACTS", getActivity()) || CommonUtils.checkPermissions("android.permission.READ_CALL_LOG", getActivity()) || CommonUtils.checkPermissions("android.permission.READ_SMS", getActivity())) {
                        status = false;
                        showErrorDialog("Required Permissions not Granted..please add permissions first.");
                    } else {*/
                        status = true;
                      //  Log.i(TAG, "clickEvent: " + status);
                        showErrorDialog("Required Permissions not Granted..please add permissions first.");
//                    }
                }
                break;
            case 7:
                status = true;
                mPresenter.updateSettings(userid, "0", "0", "0");
                break;

        }

    }


    @Override
    public void getSyncSettings() {
        mPresenter.getSyncSettings(userid);
    }

    @Override
    public void updateSyncSettings() {
        mPresenter.updateSettings(userid, contactSyn, msgSyn, callSyn);
    }

    @Override
    public void onBack() {
        //notneeded
    }

    @Override
    public void setCheckedButtons(SyncResponse.SycDataBean sycData) {
        if (sycData.getContactsync().equalsIgnoreCase("1")) {
            contactSwitch.setChecked(true);
        }
        if (sycData.getCallsync().equalsIgnoreCase("1")) {
            callSwitch.setChecked(true);
        }
        if (sycData.getSmssync().equalsIgnoreCase("1")) {
            smsSwitch.setChecked(true);
        }
    }

    @Override
    public void startFragment() {
        startActivity(new Intent(getActivity(), NavigationActivity.class));
        getActivity().finish();
    }

    @Override
    public void showErrorDialog(String msg) {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        Log.i("Checking", "showErrorDialog: " + status);
        DialogFragment newFragment = MyAlertDialogFragment.newInstance("Required Permissions missing", msg, status);
        newFragment.show(ft, "");
    }

    @Override
    public void showError(String msg) {
        Toast.makeText(getActivity(), msg, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onHideKeyboard() {

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
//        mPresenter.updateSettings(phone, contactSwitch.isChecked() ? "1" : "0", smsSwitch.isChecked() ? "1" : "0", callSwitch.isChecked() ? "1" : "0");
    }

    @Override
    public void showSuccessMsg(@StringRes int msg) {
//        Toast.makeText(getActivity(), msg, Toast.LENGTH_LONG).show();
    }

    @Override
    public boolean isInternetOk() {
        return CommonUtils.checkInternet();
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_MULTIPLE_REQUEST:
                if (grantResults.length > 0 && checkIfallGranted(grantResults)) {
                    clickEvent(getChoice());
                }
                break;
            case MY_PERMISSIONS_REQUEST_READ_CALL_LOG:
                if (grantResults.length > 0 && checkIfallGranted(grantResults)) {
                    clickEvent(getChoice());
                }
                break;
            case MY_PERMISSIONS_REQUEST_READ_SMS:
                if (grantResults.length > 0 && checkIfallGranted(grantResults)) {
                    clickEvent(getChoice());
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private boolean checkIfallGranted(int[] grantResult) {

        for (int aGrantResult : grantResult) {
            if (aGrantResult != PackageManager.PERMISSION_GRANTED) {
                return false;
            }
        }
      //  Log.i(TAG, "checkIfallGranted: true");
        return true;
    }


    @Override
    public void openAddFragment(Fragment fragment) {

    }
}
