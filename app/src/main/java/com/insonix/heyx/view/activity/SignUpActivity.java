package com.insonix.heyx.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.insonix.heyx.R;
import com.insonix.heyx.model.User;
import com.insonix.heyx.presenter.SignUpPresenter;
import com.insonix.heyx.presenter.SignUpPresenterImpl;
import com.insonix.heyx.utils.CommonProgress;
import com.insonix.heyx.utils.CommonUtils;
import com.insonix.heyx.utils.MyPreferences;

import butterknife.BindView;
import butterknife.ButterKnife;
import timber.log.Timber;

/**
 * Created by insonix on 25/11/16.
 */
public class SignUpActivity extends BaseActivity implements SignUpView {


    SignUpPresenter mSignPresenter;
    @BindView(R.id.input_name)
    EditText inputName;
    @BindView(R.id.input_password)
    EditText inputPassword;
    @BindView(R.id.input_confirmpassword)
    EditText inputConfirmpassword;
    @BindView(R.id.signup_bttn)
    Button signupBttn;
     MyPreferences mMyPrefs;
    private String mName, mCpwd, mFpwd;
    private String name;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        ButterKnife.bind(this);
        getSupportActionBar().hide();
        mMyPrefs = new MyPreferences(this);
        signupBttn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Timber.i("Clicked");
                mName = inputName.getText().toString().trim();
                mFpwd = inputPassword.getText().toString().trim();
                mCpwd = inputConfirmpassword.getText().toString().trim();
          //      mMyPrefs.createLoginSession(mName,mMyPrefs.getLoginDetails().getPhonenumber(), body.getUserid());
                if(CommonUtils.isInternetOn(SignUpActivity.this)){
                    mSignPresenter.onSubmit(mName,mFpwd, mCpwd,mMyPrefs.getToken(),getAndroidId(),mMyPrefs.getLoginDetails().getPhonenumber());
                }else{
                    CommonUtils.showNoInternetDialog(SignUpActivity.this);
                }
            }
        });
//        NOT NEEDED AS SINGLE LINE TRUE FIXED IME OPTIONS BUD WITH DIGITS

      /*  inputName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence s, int i, int i1, int i2) {
                String str = s.toString();
                if(str.length() > 0 && str.contains(" "))
                {
                    inputName.setError("Space is not allowed");
                    inputName.setText(s.toString().split(" ")[0]);
                    inputName.setSelection(s.length()-1);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });*/
    }

    @Override
    public void onShowProgress() {
        CommonProgress.showProgress(this);
    }

    @Override
    public void onHideProgress() {
        CommonProgress.hideProgress();
    }

    @Override
    public void onHideKeyboard() {

    }

    @Override
    public void showSuccessMsg(@StringRes int msg) {
        mMyPrefs.setsUSERNAME(mName);
        Log.i("Name", "showSuccessMsg: "+mName);
        mMyPrefs.setSignComplete(true);
        Snackbar.make(signupBttn,"SignUp successful",Snackbar.LENGTH_LONG).show();
    }


    @Override
    public void showNameError() {
        Snackbar.make(inputPassword, R.string.name_empty_error_msg, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showPasswordError() {
        Snackbar.make(inputPassword, R.string.pwd_empty_error_msg, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showConfirmPasswordError() {
        Snackbar.make(inputPassword, R.string.cnfrm_pwd_empty_error_msg, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showPasswordLengthError() {
        Snackbar.make(inputConfirmpassword, R.string.password_error_msg, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void showPasswordMismatchError() {
        Snackbar.make(inputConfirmpassword, R.string.pwd_mismatch_error_msg, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void moveToNext(User body) {
        name = inputName.getText().toString();
        Log.d("body","body"+body.getUserid());
        mMyPrefs.createLoginSession(name,mMyPrefs.getLoginDetails().getPhonenumber()+"",body.getUserid());

        startActivity(new Intent(SignUpActivity.this, NavigationActivity.class));
        finish();
    }

    @Override
    public void showError() {
        Snackbar.make(signupBttn,""+R.string.unknownerror,Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void setProfile(String username) {
        inputName.setText(username);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mSignPresenter = new SignUpPresenterImpl(this);
        mSignPresenter.getProfile(new MyPreferences(this).getLoginDetails().getPhonenumber());
    }

    /**
     * To fetch adroid Id
     * @return
     */


    public String getAndroidId() {
        String android_id = Settings.Secure.getString(getContentResolver(),
                Settings.Secure.ANDROID_ID);
        Log.i("dev","Device Id"+android_id);
        return android_id;
    }
}
