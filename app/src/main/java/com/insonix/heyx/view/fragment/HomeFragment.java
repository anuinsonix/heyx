package com.insonix.heyx.view.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.github.amlcurran.showcaseview.ShowcaseView;
import com.github.amlcurran.showcaseview.targets.ViewTarget;
import com.insonix.heyx.R;
import com.insonix.heyx.model.SyncResponse;
import com.insonix.heyx.utils.BackPressListenerImpl;
import com.insonix.heyx.utils.CommonUtils;
import com.insonix.heyx.utils.MyPreferences;
import com.insonix.heyx.view.activity.AddedServiceActivity;
import com.insonix.heyx.view.activity.BaseActivity;
import com.insonix.heyx.view.activity.Homeview;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class HomeFragment extends BaseFragment implements Homeview {


    @BindView(R.id.no_response)
    TextView noResponse;
    @BindView(R.id.not_available)
    LinearLayout notAvailable;
    @BindView(R.id.add_tv)
    TextView addTv;
    @BindView(R.id.imageView)
    ImageView imageView;
    @BindView(R.id.heading)
    TextView heading;
    @BindView(R.id.desc_txt)
    TextView descTxt;
    @BindView(R.id.desc)
    LinearLayout desc;
    private FragmentActivity activity;
    private View parentView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_new_home, container, false);
        ButterKnife.bind(this, view);
        getActivity().setTitle(R.string.home);
        activity = getActivity();
        ((BaseActivity) activity).setOnBackPressedListener(new BackPressListenerImpl((AppCompatActivity) activity));
      //  Log.i(TAG, "onCreateView: " + new MyPreferences(getActivity()).getToken());
        if (!isInternetOk()) {
            showUnavailable();
        }
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
//        showCase();
    }

    private void showCase() {
        new ShowcaseView.Builder(getActivity())
                .setStyle(R.style.CustomShowcaseTheme)
                .setTarget(new ViewTarget(imageView))
                .hideOnTouchOutside()
                .withNewStyleShowcase()
                .setContentTitle(R.string.add_more_services)
                .setContentText(R.string.desc_add)
                .build();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @OnClick(R.id.imageView)
    public void onClick() {

       /* AddedServiceFragment nextFrag = new AddedServiceFragment();
        this.getFragmentManager().beginTransaction()
                .add(R.id.content_navigation, nextFrag)
                .commit();*/
        startActivity(new Intent(getActivity(), AddedServiceActivity.class));
        getActivity().overridePendingTransition(R.anim.slide_right, R.anim.slide_left);
    }


    @Override
    public void showSuccessMsg(@StringRes int msg) {
        Snackbar.make(descTxt, msg, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public boolean isInternetOk() {
        return CommonUtils.checkInternet();
    }

    @Override
    public void changeView(SyncResponse.SycDataBean sycData) {

    }

    @Override
    public void showUnavailable() {
        notAvailable.setVisibility(View.VISIBLE);
    }


}
