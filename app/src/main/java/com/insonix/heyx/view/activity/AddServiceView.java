package com.insonix.heyx.view.activity;

import android.webkit.WebView;

import com.insonix.heyx.model.RequestArray;
import com.insonix.heyx.model.SyncResponse;

/**
 * Created by insonix on 25/11/16.
 */
public interface AddServiceView extends Baseview {


    boolean askForPermissions();

    boolean checkForPermissions();

    boolean checkForMsgPermission();

    void hideSelectedViews(SyncResponse.SycDataBean sycData);

    RequestArray readContacts();

    RequestArray readMsgs();

    RequestArray readCallLogs();
    void setViews(SyncResponse.SycDataBean sycData);


    void goToHome();

    void showNotAvailable();
}
