package com.insonix.heyx.view.fragment;

import com.insonix.heyx.model.SyncResponse;
import com.insonix.heyx.view.activity.Baseview;

/**
 * Created by insonix on 1/12/16.
 */
public interface SettingsView extends Baseview {

     void getSyncSettings();

     void updateSyncSettings();

     void onBack();

     void setCheckedButtons(SyncResponse.SycDataBean sycData);

     void startFragment();

     void showErrorDialog(String msg);
}
