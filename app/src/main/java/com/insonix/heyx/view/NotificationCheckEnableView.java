package com.insonix.heyx.view;

import com.insonix.heyx.model.NotificationCheckEnableResponse;
import com.insonix.heyx.model.NotificationEnableResponse;
import com.insonix.heyx.view.activity.Baseview;

/**
 * Created by insonix on 6/12/17.
 */

public interface NotificationCheckEnableView extends Baseview{
    void onNotificationcheckEnable(NotificationCheckEnableResponse notificationCheckEnableResponse);
}
