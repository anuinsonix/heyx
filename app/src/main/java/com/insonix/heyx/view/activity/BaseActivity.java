package com.insonix.heyx.view.activity;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.insonix.heyx.R;
import com.insonix.heyx.presenter.WebPresenter;
import com.insonix.heyx.presenter.WebPresenterImpl;
import com.insonix.heyx.utils.BackPressListener;
import com.insonix.heyx.utils.CommonProgress;
import com.insonix.heyx.utils.CommonUtils;
import com.insonix.heyx.view.fragment.AddServiceFragment;
import com.insonix.heyx.view.fragment.HomeFragment;

import java.util.List;

/**
 * Created by insonix on 24/11/16.
 */

public class BaseActivity extends AppCompatActivity implements Baseview {

    private static final String TAG = "BaseActivity";
    WebPresenter mPresenter;
    BackPressListener mListener;
    private boolean doubleBackToExitPressedOnce=false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setOnBackPressedListener(mListener);
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mPresenter = new WebPresenterImpl(this);
    }

    public void setOnBackPressedListener(BackPressListener onBackPressedListener) {
        this.mListener = onBackPressedListener;
    }

    @Override
    public void onBackPressed() {
        if(this instanceof AddedServiceActivity){
            super.onBackPressed();
            return;
        }
        int fragment = getSupportFragmentManager().getBackStackEntryCount();
        Fragment fragment1 = getSupportFragmentManager().findFragmentById(R.id.content_navigation);

        //should run only when stack count>1 and top fragment is not home

        if ((!(fragment1 instanceof HomeFragment) || !(fragment1 instanceof AddServiceFragment)) & fragment>1) {
            if (mListener != null)
                mListener.onCustomBackPressed(fragment1);
            else
                super.onBackPressed();
        }else{
                if (doubleBackToExitPressedOnce) {
//                    super.onBackPressed();
                    ActivityCompat.finishAffinity(this);
//                    finish();
                }
                this.doubleBackToExitPressedOnce = true;
                Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        doubleBackToExitPressedOnce = false;
                    }
                }, 3000);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
//        super.onSaveInstanceState(outState);
    }

    @Override
    public void showError(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onShowProgress() {

        CommonProgress.showProgress(this);

    }

    @Override
    public void onHideProgress() {
        CommonProgress.hideProgress();

    }


    @Override
    public void onHideKeyboard() {

    }

    @Override
    public void showSuccessMsg(@StringRes int msg) {

    }

    @Override
    public boolean isInternetOk() {
        return CommonUtils.checkInternet();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        List<Fragment> fragments = getSupportFragmentManager().getFragments();
        if (fragments != null) {
            for (Fragment fragment : fragments) {
                fragment.onRequestPermissionsResult(requestCode, permissions, grantResults);
            }
        }
    }

}
