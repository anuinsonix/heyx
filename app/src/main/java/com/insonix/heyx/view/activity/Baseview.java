package com.insonix.heyx.view.activity;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.StringRes;

/**
 * Created by insonix on 24/11/16.
 */
public interface Baseview {

    void showError(String msg);

    void onShowProgress();

    void onHideProgress();

    void onHideKeyboard();

    void showSuccessMsg(@StringRes int msg);

    boolean isInternetOk();
}
