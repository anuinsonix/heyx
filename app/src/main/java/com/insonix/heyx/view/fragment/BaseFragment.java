package com.insonix.heyx.view.fragment;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.insonix.heyx.R;
import com.insonix.heyx.presenter.WebPresenter;
import com.insonix.heyx.presenter.WebPresenterImpl;
import com.insonix.heyx.utils.CommonProgress;
import com.insonix.heyx.utils.MyApplication;
import com.insonix.heyx.view.activity.Baseview;

import butterknife.ButterKnife;

/**
 * Created by insonix on 29/11/16.
 */

public abstract class BaseFragment extends Fragment implements Baseview {


    Context ctx;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        injectViews(view);
        ctx=MyApplication.getInstance().getContext();
    }

    private void injectViews(View view) {
        ButterKnife.bind(this, view);
    }


    @Override
    public void showError(String msg) {
//        Toast.makeText(ctx, msg, Toast.LENGTH_LONG).show();
        Log.i("msg", "showError: "+msg);
   }

    @Override
    public void onShowProgress() {
        CommonProgress.showProgress(getActivity());
    }

    @Override
    public void onHideProgress() {
        CommonProgress.hideProgress();
    }

    @Override
    public void onHideKeyboard() {

    }

    @Override
    public void showSuccessMsg(@StringRes int msg) {
        Toast.makeText(ctx, msg, Toast.LENGTH_LONG).show();
    }

}
