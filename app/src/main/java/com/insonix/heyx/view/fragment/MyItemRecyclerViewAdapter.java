package com.insonix.heyx.view.fragment;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.insonix.heyx.R;
import com.insonix.heyx.model.LoggedBean;
import com.insonix.heyx.model.LoggedInData;

import java.util.List;

/**
 * {@link RecyclerView.Adapter} that can display a {@link com.insonix.heyx.model.LoggedBean} and makes a call to the
 */
public class MyItemRecyclerViewAdapter extends RecyclerView.Adapter<MyItemRecyclerViewAdapter.ViewHolder> {

    private List<LoggedInData> mValues;

    public MyItemRecyclerViewAdapter(List<LoggedInData> items) {
        mValues = items;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        LoggedInData mData = mValues.get(position);
        String address = mData.getLocality() +(mData.getCity().length()>0?",\n"+mData.getCity():"")+(mData.getRegion().length()>0?"," + mData.getRegion():"");
        holder.mIdView.setText(address);
        Log.d("address","address"+address);
        holder.mContentView.setText(mData.getLastLogin());
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public void updateData(LoggedBean logData) {

        mValues.clear();
        mValues=logData.getData();
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mIdView;
        public final TextView mContentView;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mIdView = (TextView) view.findViewById(R.id.text_address);
            mContentView = (TextView) view.findViewById(R.id.content);
        }

    }
}
