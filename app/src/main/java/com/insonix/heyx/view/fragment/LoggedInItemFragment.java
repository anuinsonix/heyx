package com.insonix.heyx.view.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.insonix.heyx.R;
import com.insonix.heyx.model.LoggedBean;
import com.insonix.heyx.model.LoggedInData;
import com.insonix.heyx.presenter.LoggedInPresenter;
import com.insonix.heyx.presenter.LoggedInPresenterImpl;
import com.insonix.heyx.utils.BackPressListenerImpl;
import com.insonix.heyx.utils.CommonUtils;
import com.insonix.heyx.utils.MyPreferences;
import com.insonix.heyx.view.activity.BaseActivity;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A fragment representing a list of last logged in sessions.
 */
public class LoggedInItemFragment extends BaseFragment implements LoggedInView {

    private static final String ARG_COLUMN_COUNT = "column_count";
    @BindView(R.id.list)
    RecyclerView list;
    @BindView(R.id.no_data)
    TextView noData;
    private int mColumnCount = 1;
    LoggedInPresenter mPresenter;
    private MyItemRecyclerViewAdapter mAdapter;
    List<LoggedInData> mData;
    private FragmentActivity activity;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public LoggedInItemFragment() {
    }

    public static LoggedInItemFragment newInstance(int columnCount) {
        LoggedInItemFragment fragment = new LoggedInItemFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
        }
        mPresenter = new LoggedInPresenterImpl(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_item_list2, container, false);
        ButterKnife.bind(this, view);
        getActivity().setTitle(R.string.web_title);
        Context context = view.getContext();
        activity = getActivity();
        ((BaseActivity)activity).setOnBackPressedListener(new BackPressListenerImpl((AppCompatActivity) activity));
        mData = new ArrayList<>();
        list.setLayoutManager(new LinearLayoutManager(context));
        mAdapter = new MyItemRecyclerViewAdapter(mData);
        list.setAdapter(mAdapter);
        mPresenter.getLastLoggedInDetails(new MyPreferences(getActivity()).getLoginDetails().getUserid());
        return view;
    }


    @Override
    public boolean isInternetOk() {
        return CommonUtils.checkInternet();
    }

    @Override
    public void showLastLoggedInSessions(LoggedBean logData) {

            mAdapter.updateData(logData);
    }

    @Override
    public void showUnavailable() {
        CommonUtils.showNoInternetDialog(getActivity());
    }

    @Override
    public void showError(String msg) {
        super.showError(msg);
        noData.setVisibility(View.VISIBLE);
    }
}
