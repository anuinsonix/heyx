package com.insonix.heyx.view.activity;

import android.annotation.TargetApi;
import android.app.ActivityManager;
import android.app.NotificationManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.provider.Settings;
import android.support.annotation.StringRes;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.insonix.heyx.BuildConfig;
import com.insonix.heyx.R;
import com.insonix.heyx.model.NotificationCheckEnableResponse;
import com.insonix.heyx.model.NotificationEnableResponse;
import com.insonix.heyx.model.SyncResponse;
import com.insonix.heyx.presenter.HomePresenter;
import com.insonix.heyx.presenter.HomePresenterImpl;
import com.insonix.heyx.presenter.NotificationCheckPresenterImpl;
import com.insonix.heyx.presenter.NotificationEnablePresenterImpl;
import com.insonix.heyx.utils.CommonProgress;
import com.insonix.heyx.utils.CommonUtils;
import com.insonix.heyx.utils.MyPreferences;
import com.insonix.heyx.utils.OnDialogClick;
import com.insonix.heyx.view.NotificationCheckEnableView;
import com.insonix.heyx.view.NotificationEnableView;
import com.insonix.heyx.view.fragment.AboutUsFragment;
import com.insonix.heyx.view.fragment.AddServiceFragment;
import com.insonix.heyx.view.fragment.HomeFragment;
import com.insonix.heyx.view.fragment.LoggedInItemFragment;
import com.insonix.heyx.view.fragment.SettingsFragment;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.content.DialogInterface.BUTTON_POSITIVE;
import static android.content.DialogInterface.OnClickListener;
import static io.fabric.sdk.android.Fabric.TAG;

public class NavigationActivity extends BaseActivity
        implements Homeview, NavigationView.OnNavigationItemSelectedListener,OnDialogClick,
        NotificationEnableView, NotificationCheckEnableView {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.content_navigation)
    FrameLayout contentNavigation;
    @BindView(R.id.nav_view)
    NavigationView navView;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;
    ActionBarDrawerToggle toggle;
    HomePresenter mPresenter;
     SwitchCompat switcher;
    String not_status;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation);
        ButterKnife.bind(this);
        mPresenter = new HomePresenterImpl(this);
        setSupportActionBar(toolbar);

        Log.i(TAG, "userid: "+new MyPreferences(this).getLoginDetails().getUserid());

        if(CommonUtils.checkInternet()){
            notificationCheckEnableHandler();


            if (getIntent().hasExtra("tag")) {
                AddServiceFragment fragment = new AddServiceFragment();
                Bundle bundle = new Bundle();
                bundle.putParcelable("tag", getIntent().getExtras().getParcelable("tag"));
                fragment.setArguments(bundle);
                openFragment(fragment);
            } else {
                mPresenter.getSelectedService(new MyPreferences(this).getLoginDetails().getUserid());
            }
        }else{
            showErrorDialog();
        }
     //  NotificationManager notificationManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
       //notificationManager.cancelAll();

        toggle = new ActionBarDrawerToggle(
                this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.setDrawerListener(toggle);
        toggle.syncState();
        navView.setNavigationItemSelectedListener(this);
//        navView.setItemIconTintList(null);
        updateDetails(navView);



    }

    private void notificationEnableHandler() {

        Menu menu = navView.getMenu();
        MenuItem menuItem = menu.findItem(R.id.notificationView);
        View actionView = MenuItemCompat.getActionView(menuItem);

        switcher = (SwitchCompat) actionView.findViewById(R.id.notification_switch);

        String userid=new MyPreferences(NavigationActivity.this).getLoginDetails().getUserid();
        NotificationEnablePresenterImpl notificationEnablePresenter=new NotificationEnablePresenterImpl(NavigationActivity.this);
        notificationEnablePresenter.enableNotification(userid,not_status);
        switcher.setChecked(true);
        switcher.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Snackbar.make(v, (switcher.isChecked()) ? "is checked!!!" : "not checked!!!", Snackbar.LENGTH_SHORT).setAction("Action", null).show();

                if(switcher.isChecked()){
                    not_status="1";
                }else {
                    not_status="2";
                }
                String userid=new MyPreferences(NavigationActivity.this).getLoginDetails().getUserid();
                NotificationEnablePresenterImpl notificationEnablePresenter=new NotificationEnablePresenterImpl(NavigationActivity.this);
                notificationEnablePresenter.enableNotification(userid,not_status);
            }
        });
    }

    private void notificationCheckEnableHandler() {

        Menu menu = navView.getMenu();
        MenuItem menuItem = menu.findItem(R.id.notificationView);
        View actionView = MenuItemCompat.getActionView(menuItem);

        switcher = (SwitchCompat) actionView.findViewById(R.id.notification_switch);
        String userid=new MyPreferences(NavigationActivity.this).getLoginDetails().getUserid();
        NotificationCheckPresenterImpl notificationCheckPresenter=new NotificationCheckPresenterImpl(this);
        notificationCheckPresenter.checkenablNotification(userid);
        /*String userid=new MyPreferences(NavigationActivity.this).getLoginDetails().getUserid();
        NotificationEnablePresenterImpl notificationEnablePresenter=new NotificationEnablePresenterImpl(NavigationActivity.this);
        notificationEnablePresenter.enableNotification(userid,not_status);
        switcher.setChecked(true);
        switcher.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Snackbar.make(v, (switcher.isChecked()) ? "is checked!!!" : "not checked!!!", Snackbar.LENGTH_SHORT).setAction("Action", null).show();

                if(switcher.isChecked()){
                    not_status="1";
                }else {
                    not_status="2";
                }
                String userid=new MyPreferences(NavigationActivity.this).getLoginDetails().getUserid();
                NotificationEnablePresenterImpl notificationEnablePresenter=new NotificationEnablePresenterImpl(NavigationActivity.this);
                notificationEnablePresenter.enableNotification(userid,not_status);

            }
        });*/
    }
    private void updateDetails(NavigationView navView) {
        TextView userName=(TextView)navView.getHeaderView(0).findViewById(R.id.name);
        userName.setText(new MyPreferences(this).getsUSERNAME());
        TextView userNumber=(TextView)navView.getHeaderView(0).findViewById(R.id.number);
        userNumber.setText(new MyPreferences(this).getLoginDetails().getPhonenumber());
    }




    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
         setIntent(intent);
        if(CommonUtils.checkInternet()){
            if (getIntent().hasExtra("tag")) {
                AddServiceFragment fragment = new AddServiceFragment();
                Bundle bundle = new Bundle();
                bundle.putParcelable("tag", getIntent().getExtras().getParcelable("tag"));
                fragment.setArguments(bundle);
                openFragment(fragment);
            } else {
                mPresenter.getSelectedService(new MyPreferences(this).getLoginDetails().getUserid());
            }
        }else{
            showErrorDialog();
        }
    }

    private void showErrorDialog() {
        AlertDialog alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setMessage("Internet not available..please try again later");
        alertDialog.setButton(BUTTON_POSITIVE, "Ok", new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                finish();
            }
        });

        alertDialog.show();
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.navigation, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();
        Fragment fragment;
        if(CommonUtils.checkInternet()){
            if (id == R.id.home) {
                mPresenter.getSelectedService(new MyPreferences(this).getLoginDetails().getUserid());
//            changeView(sycDataObj);
            }
            if (id == R.id.web) {
                setTitle(getString(R.string.web_title));
                fragment = new LoggedInItemFragment().newInstance(1);
                openFragment(fragment);

            }
            if (id == R.id.settings) {
                setTitle(getString(R.string.settings_title));
                fragment = new SettingsFragment();
                openFragment(fragment);
            }
            if (id == R.id.aboutus) {
                setTitle(getString(R.string.about_us_title));
                fragment = new AboutUsFragment();
                openFragment(fragment);
            }
            if (id == R.id.feedback) {
                CommonUtils.sendEmail();
            }if(id==R.id.notification_switch){

               // Toast.makeText(this,"Hello",Toast.LENGTH_LONG).show();
                //switcher.setChecked(!switcher.isChecked());
               /* if(switcher.isChecked()){
                    not_status="1";
                }else {
                    not_status="2";
                }
                String ph=new MyPreferences(this).getLoginDetails().getPhonenumber();
                NotificationEnablePresenterImpl notificationEnablePresenter=new NotificationEnablePresenterImpl(this);
                notificationEnablePresenter.enableNotification(ph,not_status);*/
               // Snackbar.make(item.getActionView(), (switcher.isChecked()) ? "is checked" : "not checked", Snackbar.LENGTH_SHORT).setAction("Action", null).show();

            }
       /* else {
            mPresenter.getSelectedService(new MyPreferences(this).getLoginDetails().getPhonenumber());
        }
*/
        }else{
           showErrorDialog();
        }

        DrawerLayout drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }

    private void openFragment(Fragment fragment) {
            if (!this.isFinishing()) {
                try {
                    FragmentManager fragmentManager = getSupportFragmentManager();
                    fragmentManager.beginTransaction()
                            .replace(R.id.content_navigation, fragment)
                            .setCustomAnimations(R.anim.slide_right, R.anim.slide_left)
                            .show(fragment)
                            .addToBackStack(fragment instanceof AddServiceFragment?null:String.valueOf(fragment.getId()))
                            .commitAllowingStateLoss();
                } catch (ClassCastException e) {
                    Log.e(TAG, "Can't get fragment manager");
                }
            }

    }


    @Override
    public void showError(String msg) {

    }

    @Override
    public void onShowProgress() {
      //  CommonProgress.showProgress(this);
    }

    @Override
    public void onHideProgress() {
      //  CommonProgress.hideProgress();

    }
    @Override
    public void onHideKeyboard() {

    }

    @Override
    public void showSuccessMsg(@StringRes int msg) {

    }


    @Override
    public void changeView(SyncResponse.SycDataBean sycData) {
//        sycDataObj = sycData;
        if (sycData != null && sycData.getCallsync().equalsIgnoreCase("0") && sycData.getSmssync().equalsIgnoreCase("0") && sycData.getContactsync().equalsIgnoreCase("0")) {
            setTitle("Home");
            openFragment(new HomeFragment());
        } else {
            setTitle("Home");
            openFragment(new AddServiceFragment());
        }
    }

    @Override
    public void showUnavailable() {

    }


    @Override
    public void openAddFragment(Fragment fragment) {
        openFragment(fragment);
    }

    @Override
    public void onNotificationEnable(NotificationEnableResponse notificationEnableResponse) {
        onHideProgress();
        Toast.makeText(this,notificationEnableResponse.getMessage(),Toast.LENGTH_LONG).show();
    }


    @Override
    public void onNotificationcheckEnable(NotificationCheckEnableResponse notificationCheckEnableResponse) {
        //Toast.makeText(this,notificationCheckEnableResponse.getNotiStatus(),Toast.LENGTH_LONG).show();
        notificationEnableHandler();
        if(notificationCheckEnableResponse.getNotiStatus().equals("1")){
            Log.d(TAG,"notiswitch"+notificationCheckEnableResponse.getNotiStatus());
         //   Toast.makeText(this,notificationCheckEnableResponse.getNotiStatus(),Toast.LENGTH_LONG).show();
            switcher.setChecked(true);
        }
        else{
            switcher.setChecked(false);
        }

    }
}
