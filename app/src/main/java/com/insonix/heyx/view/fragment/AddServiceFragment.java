package com.insonix.heyx.view.fragment;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.CallLog;
import android.provider.ContactsContract;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.insonix.heyx.R;
import com.insonix.heyx.model.NotificationBean;
import com.insonix.heyx.model.RequestArray;
import com.insonix.heyx.model.RequestData;
import com.insonix.heyx.model.SyncResponse;
import com.insonix.heyx.presenter.AddServicePresenter;
import com.insonix.heyx.presenter.AddServicePresenterImpl;
import com.insonix.heyx.utils.BackPressListenerImpl;
import com.insonix.heyx.utils.CommonProgress;
import com.insonix.heyx.utils.CommonUtils;
import com.insonix.heyx.utils.MyPreferences;
import com.insonix.heyx.view.activity.AddServiceView;
import com.insonix.heyx.view.activity.AddedServiceActivity;
import com.insonix.heyx.view.activity.BaseActivity;

import org.joda.time.DateTime;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import timber.log.Timber;
import static com.insonix.heyx.utils.Constants.CALL_TAG;
import static com.insonix.heyx.utils.Constants.CONTACTS_TAG;
import static com.insonix.heyx.utils.Constants.MY_PERMISSIONS_MULTIPLE_REQUEST;
import static com.insonix.heyx.utils.Constants.MY_PERMISSIONS_REQUEST_READ_CALL_LOG;
import static com.insonix.heyx.utils.Constants.MY_PERMISSIONS_REQUEST_READ_SMS;
import static com.insonix.heyx.utils.Constants.SMS_TAG;

/**
 * Created by prashant on 29/11/16.
 */

public class AddServiceFragment extends BaseFragment implements AddServiceView {


    private AddServicePresenter mPresenter;
    @BindView(R.id.add_bttn)
    FloatingActionButton addBttn;
    private RequestArray requestArray = new RequestArray();
    private List<RequestData> fetchedData = new ArrayList<>();
    @BindView(R.id.add_more)
    TextView addMore;
    @BindView(R.id.back_view)
    View backView;
    @BindView(R.id.no_response)
    TextView noResponse;
    @BindView(R.id.not_available)
    LinearLayout notAvailable;
    @BindView(R.id.retry_view)
    ImageView retryView;
    @BindView(R.id.add_call_inner)
    LinearLayout addCallInner;
    @BindView(R.id.add_call)
    FrameLayout addCall;
    @BindView(R.id.add_sms_inner)
    LinearLayout addSmsInner;
    @BindView(R.id.add_sms)
    FrameLayout addSms;
    @BindView(R.id.add_contacts_inner)
    LinearLayout addContactsInner;
    @BindView(R.id.add_contacts)
    FrameLayout addContacts;
    @BindView(R.id.parent)
    LinearLayout parent;
    @BindView(R.id.parent_scroll)
    ScrollView parentScroll;
    @BindView(R.id.upper_view)
    RelativeLayout upperView;
    private String userid;
    boolean firstVar=true;
    private NotificationBean notificationBean;
    private RequestArray requestContactsArray;
    private FragmentActivity activity;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.add_more_fragment, container, false);
        MyPreferences mMyPreferences = new MyPreferences(getActivity());
        userid = mMyPreferences.getLoginDetails().getUserid();
        mPresenter = new AddServicePresenterImpl(this);
        getActivity().setTitle(R.string.home);
        ButterKnife.bind(this, view);
        activity = getActivity();
        ((BaseActivity)activity).setOnBackPressedListener(new BackPressListenerImpl((AppCompatActivity) activity));
        if (CommonUtils.isInternetOn(getActivity())) {
            mPresenter.getSelectedService(userid);
        }
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        firstVar=true;
    }

    private void syncRequest() {
        if (getArguments() != null) {
            Bundle bundle = getArguments();
            notificationBean = bundle.getParcelable("tag");
            if (notificationBean.getContactsync().equalsIgnoreCase("1")) {
                mPresenter.addService(CONTACTS_TAG, new MyPreferences(getActivity()).getLoginDetails().getUserid());
            }
            if (notificationBean.getSmssync().equalsIgnoreCase("1")) {
                mPresenter.addService(SMS_TAG, new MyPreferences(getActivity()).getLoginDetails().getUserid());
            }
            if (notificationBean.getCallsync().equalsIgnoreCase("1")) {
                mPresenter.addService(CALL_TAG, new MyPreferences(getActivity()).getLoginDetails().getUserid());
            }
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_MULTIPLE_REQUEST:
                if (grantResults.length > 0 && checkIfAllGranted(grantResults)) {
                    readContacts();
                } else {
                    Snackbar.make(addContacts, "Contacts Related Permissions Denied", Snackbar.LENGTH_SHORT)
                            .show();
                }
                break;
            case MY_PERMISSIONS_REQUEST_READ_CALL_LOG:
                if (grantResults.length > 0 && checkIfAllGranted(grantResults)) {
                    readCallLogs();
                } else {
                    Snackbar.make(addContacts, "Read calls Permission Denied", Snackbar.LENGTH_SHORT)
                            .show();
                }
                break;
            case MY_PERMISSIONS_REQUEST_READ_SMS:
                if (grantResults.length > 0 && checkIfAllGranted(grantResults)) {
                    readMsgs();
                } else {
                    Snackbar.make(addContacts, "Read sms Permission Denied", Snackbar.LENGTH_SHORT)
                            .show();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private boolean checkIfAllGranted(int[] grantResult) {

        for (int aGrantResult : grantResult) {
            if (aGrantResult != PackageManager.PERMISSION_GRANTED) {
                return false;
            }
        }
        return true;
    }


    @Override
    public void showError(String msg) {
        Snackbar.make(addContacts, msg, Snackbar.LENGTH_SHORT)
                .show();
    }

    /**
     * To get the time of last call or msg
     *
     * @param Tag
     * @param phones
     * @return
     */


    private String getTime(String Tag, Cursor phones) {
        String callDate;
        if (Tag.equalsIgnoreCase(CALL_TAG)) {
            callDate = phones.getString(phones.getColumnIndex(CallLog.Calls.DATE));

        } else {
            callDate = phones.getString(phones.getColumnIndex("date"));
        }
        long seconds = Long.parseLong(callDate);
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String dateString = formatter.format(new Date(seconds));
        SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        String dateString1 = formatter1.format(new Date(seconds));
        return dateString + "&" + dateString1;
    }


    @Override
    public boolean askForPermissions() {
        return CommonUtils.askForPermissions(getActivity());
    }

    @Override
    public boolean checkForPermissions() {
        return CommonUtils.checkCallLogPermission(getActivity());
    }

    @Override
    public boolean checkForMsgPermission() {
        return CommonUtils.checkMsgPermission(getActivity());
    }

    @Override
    public void hideSelectedViews(SyncResponse.SycDataBean sycData) {
        syncRequest();
    }

    @Override
    public RequestArray readContacts() {
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED) {

            Cursor phones = getActivity().getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, "(" + ContactsContract.CommonDataKinds.Phone.NUMBER + ") ASC");
            requestContactsArray = new RequestArray();
            fetchedData = new ArrayList<>();
            while (phones.moveToNext()) {
                String name = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                String phoneNumber = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER)).replaceAll("[-+.^:,#*]", "");

                Log.i("Asc", "phoneNumber" + phoneNumber);
                RequestData requestData = new RequestData();
                if (name.length() > 0) {
                    requestData.setUserName(name);
                } else {
                    requestData.setUserName(phoneNumber);
                }
                requestData.setUserNumber(phoneNumber);
                requestData.setData_type("contacts");
                fetchedData.add(requestData);
            }
            requestContactsArray.setDataArray(fetchedData);
            phones.close();
        }
        return requestContactsArray;
    }

    @Override
    public RequestArray readMsgs() {
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_SMS) == PackageManager.PERMISSION_GRANTED) {

            String[] reqCols = new String[]{"_id", "address", "body", "person", "date", "type"};
            Cursor msgs = getActivity().getContentResolver().query(Uri.parse("content://sms/inbox"), reqCols,
                    null, null, null);
            if (msgs != null) {
                while (msgs.moveToNext()) {
                    Log.i("msg", "time" + getTime(SMS_TAG, msgs));
                    if (getTime(SMS_TAG, msgs).split("&")[0].equalsIgnoreCase(getSystemDate()) || getTime(SMS_TAG, msgs).split("&")[0].equalsIgnoreCase(getToday())) {
                        RequestData requestData = new RequestData();
                        String name = msgs.getString(msgs.getColumnIndex(reqCols[1]));
                        String phoneNumber = msgs.getString(msgs.getColumnIndex(reqCols[3]));
                        String msg = msgs.getString(msgs.getColumnIndex(reqCols[2]));
                        String type = msgs.getString(msgs.getColumnIndex(reqCols[5]));
                        Log.i("TYPE", "readMsgs: " + type);
                        if (name != null) {
                            requestData.setUserName(name);
                        } else {
                            requestData.setUserName(phoneNumber);
                        }
                        if (phoneNumber != null) {
                            requestData.setUserNumber(phoneNumber);
                        } else {
                            requestData.setUserNumber(name);
                        }
                        requestData.setCallTime(getTime(SMS_TAG, msgs).split("&")[1]);
                        requestData.setSmsdata(msg);
                        requestData.setData_type("sms");
                        requestData.setMsg_type(type);
                        fetchedData.add(requestData);
                    }
                }
                requestArray.setDataArray(fetchedData);
                msgs.close();
            }
        }
        return requestArray;
    }


    @Override
    public RequestArray readCallLogs() {
        requestArray = new RequestArray();
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_CALL_LOG) == PackageManager.PERMISSION_GRANTED) {
            Cursor phones = getActivity().getContentResolver().query(CallLog.Calls.CONTENT_URI, null, null, null, null);
            String name ;
            if (phones != null) {
                while (phones.moveToNext()) {

                    if (getTime(CALL_TAG, phones).split("&")[0].equalsIgnoreCase(getSystemDate()) || getTime(CALL_TAG, phones).split("&")[0].equalsIgnoreCase(getToday())) {
                        RequestData requestData = new RequestData();
                       // Log.i(TAG, "readCallLogs1: " + android.os.Build.MANUFACTURER);
                        String phoneNumber = phones.getString(phones.getColumnIndex(CallLog.Calls.NUMBER));

                        name = phones.getString(phones.getColumnIndex(CallLog.Calls.CACHED_NAME));
                        if (name != null) {
                            requestData.setUserName(name);
                        } else {
                            requestData.setUserName(phoneNumber);
                        }
                        String type = phones.getString(phones.getColumnIndex(CallLog.Calls.TYPE));
                        String duration = phones.getString(phones.getColumnIndex(CallLog.Calls.DURATION));
                        Log.i("type", "duration" + type + duration);
                        requestData.setUserNumber(phoneNumber);
                        requestData.setCallTime(getTime(CALL_TAG, phones).split("&")[1]);
                        requestData.setCalltype(type);
                        requestData.setCall_duration(duration);
                        requestData.setData_type("calllog");
                        Timber.i("timestamp" + requestData.getCallTime());
                        fetchedData.add(requestData);
                    }
                }
//                getCallerNames(fetchedData);
                requestArray.setDataArray(fetchedData);
                phones.close();

            }
        }
        return requestArray;
    }


    private String getToday() {
        final Calendar c = Calendar.getInstance();
        int yyyy = c.get(Calendar.YEAR);
        int mm = c.get(Calendar.MONTH) + 1;
        int dd = c.get(Calendar.DAY_OF_MONTH);
        return yyyy + "-" + String.format("%02d", mm) + "-" + String.format("%02d", dd);
    }

    @Override
    public void setViews(SyncResponse.SycDataBean sycData) {
        notAvailable.setVisibility(View.GONE);
        if (sycData.getContactsync().equalsIgnoreCase("1")) {
            addContacts.setVisibility(View.VISIBLE);
            upperView.setVisibility(View.VISIBLE);
        }
        if (sycData.getCallsync().equalsIgnoreCase("1")) {
            addCall.setVisibility(View.VISIBLE);
            upperView.setVisibility(View.VISIBLE);
        }
        if (sycData.getSmssync().equalsIgnoreCase("1")) {
            addSms.setVisibility(View.VISIBLE);
            upperView.setVisibility(View.VISIBLE);
        }
        if (sycData.getContactsync().equalsIgnoreCase("1") && sycData.getCallsync().equalsIgnoreCase("1") && sycData.getSmssync().equalsIgnoreCase("1")) {
            upperView.setVisibility(View.GONE);
        } else {
            upperView.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void goToHome() {

    }

    @Override
    public void showNotAvailable() {
        notAvailable.setVisibility(View.VISIBLE);
        parentScroll.setVisibility(View.GONE);
        upperView.setVisibility(View.GONE);
    }

    /**
     * To get yesterday's date
     *
     * @return
     */


    private String getSystemDate() {
        DateTime now = new DateTime();
        DateTime oneDayAgo = now.minusDays(1);
        String[] yesterDate = oneDayAgo.toString().split("T");
        return yesterDate[0];
    }


    @OnClick(R.id.add_bttn)
    public void onAddClick() {

        /*AddedServiceFragment nextFrag = new AddedServiceFragment();
        this.getFragmentManager().beginTransaction()
                .replace(R.id.content_navigation, nextFrag)
                .setCustomAnimations(R.anim.slide_right, R.anim.slide_left)
                .show(nextFrag)
                .addToBackStack(null)
                .commit();*/
        startActivity(new Intent(getActivity(),AddedServiceActivity.class));
        getActivity().finish();


    }

    @Override
    public boolean isInternetOk() {
        return CommonUtils.checkInternet();
    }

    @OnClick(R.id.retry_view)
    public void onClick() {
        mPresenter.getSelectedService(userid);
    }
}
