package com.insonix.heyx.service;



import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.insonix.heyx.utils.MyPreferences;

/**
 * Created by insonix on 30/11/16.
 */
//Class extending FirebaseInstanceIdService
public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {

    private static final String TAG = "MyFirebaseIIDService";

    @Override
    public void onTokenRefresh() {

        //Getting registration token
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        setRegistrationToServer(refreshedToken);
        //Displaying token on logcat
        Log.d(TAG, "Refreshed token: " + refreshedToken);

    }

    private void setRegistrationToServer(String token) {
        MyPreferences mPrefs=new MyPreferences(this);
        mPrefs.setToken(token);
    }
}