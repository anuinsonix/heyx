package com.insonix.heyx.api;

import com.insonix.heyx.model.GenericResponse;
import com.insonix.heyx.model.LoggedBean;
import com.insonix.heyx.model.NotificationCheckEnableResponse;
import com.insonix.heyx.model.NotificationEnableResponse;
import com.insonix.heyx.model.Profile;
import com.insonix.heyx.model.SyncResponse;
import com.insonix.heyx.model.User;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

/**
 * Created by insonix on 28/11/16.
 */

public interface ApiEndpointInterface {
    // Request method and URL specified in the annotation
    // Callback for the parsed response is the last parameter

    @POST("register.php")
    Call<User> registerUser(@Query("username") String username, @Query("phonenumber") String phonenumber, @Query("password") String password, @Query("device_type") String os, @Query("device_token") String token, @Query("android_id") String android_id);


    @Multipart
    @POST("savedata.php")
    Call<GenericResponse> addService(@Part("cmd") RequestBody cmd, @Part("user_id") RequestBody userid, @Part MultipartBody.Part file);

    @GET("getsynchronize.php")
    Call<SyncResponse> getSyncSettings(@Query("user_id") String userid);

    @GET("profile.php")
    Call<Profile> getProfile(@Query("phone_number") String phonenumber);

    @GET("loguserdetails.php")
    Call<LoggedBean> getLoggedInDetails(@Query("user_id") String userid);

    @POST("updatesynchronize.php")
    Call<GenericResponse> setSyncSettings(@Query("user_id") String userid,@Query("contactsync") String contsyn,@Query("smssyn") String smssync,@Query("callsync") String callsync);

    @GET("enableNotification.php")
    Call<NotificationEnableResponse> enableNotification(@Query("user_id") String userid, @Query("noti_status") String noti_status);

    @GET("getNotiEnablestatus.php")
    Call<NotificationCheckEnableResponse> checkenablNotification(@Query("user_id") String userid);

}
